<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/ReportedArticle.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
$sellerName = $userData->getName();

$articles = getArticles($conn, "WHERE author_uid =? AND type = 'Reported' ",array("author_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Reported Articles | The company" />
<title>Reported Articles | The company</title>
<meta property="og:description" content="The company serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into The company to search for their preferred breed or getting advice from us." />
<meta name="description" content="The company serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into The company to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="The company, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="left-h1-div">
            <h1 class="green-text h1-title">Reported Articles</h1>
            <div class="green-border"></div>
        </div>
        <div class="mid-search-div">
        	<form>
            <input class="line-input clean" type="text" placeholder="Search" id="myInput" onkeyup="myFunction()">
                <button class="search-btn hover1 clean">
                        <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                        <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                </button>
            </form>
        </div>
        <div class="right-add-div">
        	<a href="sellerAddArticle.php"><div class="green-button white-text puppy-button">Add Article</div></a>
        </div>
    
    </div>


    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100" id="myTable">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Author</th>
                    <th>Article Title</th>
                    <th>Approved On</th>
                    <th>Reported On</th>
                    <th>Reported By</th>
                    <th>Reason</th>
                    <th>Admin Decision</th>
                </tr>
            </thead>

            <tbody>
                <?php
                    if($articles)
                    {
                        for($cnt = 0;$cnt < count($articles) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?>.</td>
                                <td><?php echo $articles[$cnt]->getAuthorName();?></td>
                                <td><?php echo $articles[$cnt]->getTitle();?></td>
                                <td><?php echo $date = date("d/m/Y",strtotime($articles[$cnt]->getDateUpdated()));?></td>

                                <td>
                                    <?php 
                                        $conn = connDB();
                                        $reportId = $articles[$cnt]->getImgFiveSrc();
                                        $uplineDetails = getReportedArticle($conn,"WHERE uid = ? ", array("uid") ,array($reportId),"s");
                                        $uplineRows = $uplineDetails[0];  
                                        echo $dateReport = date("d/m/Y h:i:s",strtotime($uplineRows->getDateUpdated()));
                                        // echo $dateReport = date("d/m/Y",strtotime($uplineRows->getDateUpdated()));
                                        // echo $uplineRows->getDateUpdated();
                                        $conn->close();
                                    ?>
                                </td>

                                <td><?php echo $articles[$cnt]->getAuthor();?></td>
                                <td><?php echo $uplineRows->getReason();?></td>
                                <td>
                                    <?php 
                                        $decision = $articles[$cnt]->getType();
                                        if($decision == 'Reported')
                                        {
                                            echo "Pending";
                                        }
                                        else
                                        {
                                            echo "Keep / Delete";
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>    
            </tbody>

            <!-- <tbody>
            	<tr>
                	<td class="first-column">1.</td>
                    <td>Pet Seller 1</td>
                    <td>XXXXXXXXXXX</td>
                    <td>1/12/2019</td>
                    <td>1/12/2019</td>
                    <td>
                        <div class="table-left-review-profile">
                            <img src="img/profile-pic.jpg" class="profile-pic-css">
                        </div>                    
                        <div class="table-left-review-data">
                            <p class="table-review-username-p">User 1</p>
                        </div>               
                        <div class="clear"></div>     
                        <div class="table-review-comment">
                               <p class="table-review-p">Spam</p>
                               <p class="table-date-p">12/12/2019</p>
                        </div>  
                    </td>
                    <td>
                    	Pending                   
                    </td>
                </tr>
            	<tr>
                	<td class="first-column">2.</td>
                    <td>Pet Seller 1</td>
                    <td>XXXXXXXXXXX</td>
                    <td>1/12/2019</td>
                    <td>1/12/2019</td>
                    <td>
                        <div class="table-left-review-profile">
                            <img src="img/profile-pic.jpg" class="profile-pic-css">
                        </div>                    
                        <div class="table-left-review-data">
                            <p class="table-review-username-p">User 1</p>
                        </div>               
                        <div class="clear"></div>     
                        <div class="table-review-comment">
                               <p class="table-review-p">Spam</p>
                               <p class="table-date-p">12/12/2019</p>
                        </div>  
                    </td>
                    <td>
                    	Pending                    
                    </td>
                </tr>
            	<tr>
                	<td class="first-column">3.</td>
                    <td>Pet Seller 1</td>
                    <td>XXXXXXXXXXX</td>
                    <td>1/12/2019</td>
                    <td>1/12/2019</td>
                    <td>
                        <div class="table-left-review-profile">
                            <img src="img/profile-pic.jpg" class="profile-pic-css">
                        </div>                    
                        <div class="table-left-review-data">
                            <p class="table-review-username-p">User 1</p>
                        </div>               
                        <div class="clear"></div>     
                        <div class="table-review-comment">
                               <p class="table-review-p">Spam</p>
                               <p class="table-date-p">12/12/2019</p>
                        </div>  
                    </td>
                    <td>
                    	Pending                    
                    </td>
                </tr>                                
            </tbody> -->
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>