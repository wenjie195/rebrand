<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$timestamp = time();

if(isset($_POST["image"]))
{
	$data = $_POST["image"];

	$image_array_1 = explode(";", $data);

	$image_array_2 = explode(",", $image_array_1[1]);

	$data = base64_decode($image_array_2[1]);

	// $imageName = time() . '.png';
	$imageName = $uid.time() . '.png';

	file_put_contents('uploads/'.$imageName, $data);

	if(isset($_POST["image"]))
	{
		$tableName = array();
		$tableValue =  array();
		$stringType =  "";
		//echo $imageName;

		if($imageName)
		{
		array_push($tableName,"company_pic");
		array_push($tableValue,$imageName);
		$stringType .=  "s";
		}

		array_push($tableValue,$uid);
		$stringType .=  "s";
		$uploadCompanyLogo = updateDynamicData($conn,"seller"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
		if($uploadCompanyLogo)
		{
		?>
			<div class="preview-img-div">
				<img src=uploads/<?php echo $imageName  ?>  class="width100" />
			</div>
		<?php
		?>

		<h4><?php echo "Image Preview"; ?></h4>

		<form method="post" action="profilePreview.php"> 
			<div class="width100 overflow text-center">     
				<button class="green-button white-text clean2 edit-1-btn margin-auto">Complete</button>
			</div>
		</form>

		<?php
		}
		else
		{
			echo "fail";
		}

	}


}

?>