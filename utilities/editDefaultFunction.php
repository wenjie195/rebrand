<?php 
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

if(isset($_SESSION['petsType'])) {
	$petsType = $_SESSION['petsType'];
}

// $uid = $_SESSION['newPetsUid'];
// $defaultImage = $_POST['default_image'];
// $puppyUpdated = updateDynamicData($conn,"puppy","WHERE uid =?",array("default_image"),array($defaultImage,$uid), "ss");

// if ($puppyUpdated) {
// 	$_SESSION['messageType'] = 1;
// 	header('location: ../allPuppies.php');
// }else{
// 	echo "error";
// }
// unset($_SESSION['puppy_uid']);unset($_SESSION['newPetsUid']);unset($_SESSION['image']);

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST['uid']);
    $defaultImage = $_POST['default_image'];


    //   FOR DEBUGGING 
    //  echo "<br>";
    // echo $uid."<br>";
    // echo $defaultImage."<br>";
}

if(isset($_POST['defaultImageSubmit']))
{   
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    //echo "save to database";
    if($defaultImage)
    {
        array_push($tableName,"default_image");
        array_push($tableValue,$defaultImage);
        $stringType .=  "s";
    }

    array_push($tableValue,$uid);
    $stringType .=  "s";
    if($petsType == 1){
        $updateProductDetails = updateDynamicData($conn,"puppy"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        $updateProductDetails = updateDynamicData($conn,"pet_details"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updateProductDetails)
        { 
            $_SESSION['messageType'] = 1;
            echo "<script>alert('Data Updated and Stored !');window.location='../allPuppies.php'</script>"; 
        }
        else
        {      
            $_SESSION['messageType'] = 1;
            echo "<script>alert('Fail to Update Data !');window.location='../allPuppies.php'</script>"; 
        }
    }
    elseif($petsType == 2){
        $updateProductDetails = updateDynamicData($conn,"kitten"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        $updateProductDetails = updateDynamicData($conn,"pet_details"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updateProductDetails)
        { 
            $_SESSION['messageType'] = 1;
            echo "<script>alert('Data Updated and Stored !');window.location='../allKittens.php'</script>"; 
        }
        else
        {      
            $_SESSION['messageType'] = 1;
            echo "<script>alert('Fail to Update Data !');window.location='../allKittens.php'</script>"; 
        }
    }
    elseif($petsType == 3){
        $updateProductDetails = updateDynamicData($conn,"reptile"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        $updateProductDetails = updateDynamicData($conn,"pet_details"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updateProductDetails)
        { 
            $_SESSION['messageType'] = 1;
            echo "<script>alert('Data Updated and Stored !');window.location='../allReptiles.php'</script>"; 
        }
        else
        {      
            $_SESSION['messageType'] = 1;
            echo "<script>alert('Fail to Update Data !');window.location='../allReptiles.php'</script>"; 
        }
    }
}
else
{
    header('Location: ../index.php');
    // $_SESSION['messageType'] = 1;
    //header('Location: ../editProfile.php?type=1');
}
 ?>