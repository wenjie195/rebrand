<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../sellerAccess.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Seller.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    // $uid = $_SESSION['uid'];
    $uid = rewrite($_POST["company_uid"]);

    $name = rewrite($_POST["update_name"]);
    $slug = rewrite($_POST["update_slug"]);
    $registerNo = rewrite($_POST["update_regno"]);
    $compContact = rewrite($_POST["update_comp_contact"]);
    $compAdd = rewrite($_POST["update_comp_add"]);
    $compState = rewrite($_POST["update_comp_state"]);
    $pic = rewrite($_POST["update_pic"]);
    $picHp = rewrite($_POST["update_pic_hp"]);
    $experience = rewrite($_POST["update_exp"]);
    $cert = rewrite($_POST["update_cert"]);
    $breed = rewrite($_POST["update_breed"]);
    $info = rewrite($_POST["update_info"]);

    $email = rewrite($_POST["update_email"]);

    // $oldLogo = rewrite($_POST["old_logo"]);
    // $oldPic = rewrite($_POST["old_pic"]);

    // // $imgOne = rewrite($_POST['update_image_one']);
    // $imgOne = $_FILES['update_image_one']['name'];
    // if($imgOne != "")
    // {
    //     $imageOne = $timestamp.$uid.$_FILES['update_image_one']['name'];
    //     $target_dir = "../uploads/";
    //     $target_file = $target_dir . basename($_FILES["update_image_one"]["name"]);
    //     // Select file type
    //     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    //     // Valid file extensions
    //     $extensions_arr = array("jpg","jpeg","png","gif");
    //     if( in_array($imageFileType,$extensions_arr) )
    //     {
    //     move_uploaded_file($_FILES['update_image_one']['tmp_name'],$target_dir.$imageOne);
    //     }
    // }
    // else
    // {    
    //     $imageOne = rewrite($_POST["old_logo"]);
    // }

    // // $imgTwo = rewrite($_POST['update_image_two']);
    // $imgTwo = $_FILES['update_image_two']['name'];
    // if($imgTwo != "")
    // {
    //     $imageTwo = $timestamp.$uid.$_FILES['update_image_two']['name'];
    //     $target_dir = "../uploads/";
    //     $target_file = $target_dir . basename($_FILES["update_image_two"]["name"]);
    //     // Select file type
    //     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    //     // Valid file extensions
    //     $extensions_arr = array("jpg","jpeg","png","gif");
    //     if( in_array($imageFileType,$extensions_arr) )
    //     {
    //     move_uploaded_file($_FILES['update_image_two']['tmp_name'],$target_dir.$imageTwo);
    //     }
    // }
    // else
    // {    
    //     $imageTwo = rewrite($_POST["old_pic"]);
    // }

    //   ORIGINAL FUNCTION 
    // $oldFavorite = $_POST["petri_old"];
    // $oldFavoriteExp = explode(",",$oldFavorite);
    $newFavorite = $_POST["petri"];
    $newFavoriteImp = implode(",",$newFavorite);
    // if ($oldFavorite)
    // {
    //     $ArrImplode = $oldFavorite.",".$newFavorite;
    // }
    // else 
    // {
    //     $ArrImplode = $newFavorite;
    // }

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";
    // echo $newFavoriteImp."<br>";

    $seller = getSeller($conn," uid = ? ",array("uid"),array($uid),"s");    

    if(!$seller)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"company_name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        if($slug)
        {
            array_push($tableName,"slug");
            array_push($tableValue,$slug);
            $stringType .=  "s";
        }
        if($registerNo)
        {
            array_push($tableName,"registration_no");
            array_push($tableValue,$registerNo);
            $stringType .=  "s";
        }
        if($compContact)
        {
            array_push($tableName,"contact_no");
            array_push($tableValue,$compContact);
            $stringType .=  "s";
        }
        if($compAdd)
        {
            array_push($tableName,"address");
            array_push($tableValue,$compAdd);
            $stringType .=  "s";
        }
        if($compState)
        {
            array_push($tableName,"state");
            array_push($tableValue,$compState);
            $stringType .=  "s";
        }
        if($pic)
        {
            array_push($tableName,"contact_person");
            array_push($tableValue,$pic);
            $stringType .=  "s";
        }
        if($picHp)
        {
            array_push($tableName,"contact_person_no");
            array_push($tableValue,$picHp);
            $stringType .=  "s";
        }
        if($experience)
        {
            array_push($tableName,"experience");
            array_push($tableValue,$experience);
            $stringType .=  "s";
        }
        if($cert)
        {
            array_push($tableName,"cert");
            array_push($tableValue,$cert);
            $stringType .=  "s";
        }
        if($breed)
        {
            array_push($tableName,"breed_type");
            array_push($tableValue,$breed);
            $stringType .=  "s";
        }
        if($info)
        {
            array_push($tableName,"other_info");
            array_push($tableValue,$info);
            $stringType .=  "s";
        }

        // if($imageOne)
        // {
        //     array_push($tableName,"company_logo");
        //     array_push($tableValue,$imageOne);
        //     $stringType .=  "s";
        // }
        // if($imageTwo)
        // {
        //     array_push($tableName,"company_pic");
        //     array_push($tableValue,$imageTwo);
        //     $stringType .=  "s";
        // }

        if($newFavoriteImp)
        {
            array_push($tableName,"services");
            array_push($tableValue,$newFavoriteImp);
            $stringType .=  "s";
        }

        // if($ArrImplode)
        // {
        //     array_push($tableName,"services");
        //     array_push($tableValue,$ArrImplode);
        //     $stringType .=  "s";
        // }

        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $profileUpdated = updateDynamicData($conn,"seller"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($profileUpdated)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../profilePreview.php?type=1');

            if(isset($_POST['submit']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($name)
                {
                    array_push($tableName,"name");
                    array_push($tableValue,$name);
                    $stringType .=  "s";
                }
                if($compContact)
                {
                    array_push($tableName,"phone_no");
                    array_push($tableValue,$compContact);
                    $stringType .=  "s";
                }
                if($email)
                {
                    array_push($tableName,"email");
                    array_push($tableValue,$email);
                    $stringType .=  "s";
                }
                
                array_push($tableValue,$uid);
                $stringType .=  "s";
                $updateKittenDetails = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    
                if($updateKittenDetails)
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../profilePreview.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../editSellerProfile.php?type=2');
                }

            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../editSellerProfile.php?type=3');
            }

        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editSellerProfile.php?type=4');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../editSellerProfile.php?type=5');
    }

}
else 
{
    header('Location: ../index.php');
}
?>
