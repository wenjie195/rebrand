<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST['update_product_uid']);
    $freeGift= rewrite($_POST['update_free_gift']);
    $freeGiftDesc = rewrite($_POST['update_freeGift_desc']);


    //   FOR DEBUGGING 
    //  echo "<br>";
    // echo $uid."<br>";
    // echo $freeGift."<br>";
    // echo $freeGiftDesc ."<br>";
}

if(isset($_POST['editSubmit']))
{   
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    //echo "save to database";
    if($freeGift)
    {
        array_push($tableName,"free_gift");
        array_push($tableValue,$freeGift);
        $stringType .=  "s";
    }

    if($freeGiftDesc)
    {
        array_push($tableName,"free_gift_description");
        array_push($tableValue,$freeGiftDesc);
        $stringType .=  "s";
    }


    array_push($tableValue,$uid);
    $stringType .=  "s";
    $updateProductDetails = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    if($updateProductDetails)
    { 
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Data Updated and Stored !');window.location='../allProduct.php'</script>"; 
    }
    else
    {      
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Fail to Update Data !');window.location='../allProduct.php'</script>"; 
    }
}
else
{
    header('Location: ../index.php');
    // $_SESSION['messageType'] = 1;
    //header('Location: ../editProfile.php?type=1');
}

?>