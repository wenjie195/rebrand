<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_SESSION['uid'];

    $receiverName = rewrite($_POST["update_receiver_name"]);
    $receiverContactNo = rewrite($_POST["update_receiver_phone"]);
    $shippingAddress = rewrite($_POST["update_address"]);
    $shippingArea = rewrite($_POST["update_area"]);
    $shippingPostalCode = rewrite($_POST["update_code"]);
    $shippingState = rewrite($_POST["update_state"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";
    // echo $gender."<br>";
    // echo $dob."<br>";
    // echo $phone."<br>";
    // echo $email."<br>";

    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");    

    if(!$user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($receiverName)
        {
            array_push($tableName,"receiver_name");
            array_push($tableValue,$receiverName);
            $stringType .=  "s";
        }
        if($receiverContactNo)
        {
            array_push($tableName,"receiver_contact_no");
            array_push($tableValue,$receiverContactNo);
            $stringType .=  "s";
        }
        if($shippingAddress)
        {
            array_push($tableName,"shipping_address");
            array_push($tableValue,$shippingAddress);
            $stringType .=  "s";
        }
        if($shippingArea)
        {
            array_push($tableName,"shipping_area");
            array_push($tableValue,$shippingArea);
            $stringType .=  "s";
        }
        if($shippingPostalCode)
        {
            array_push($tableName,"shipping_postal_code");
            array_push($tableValue,$shippingPostalCode);
            $stringType .=  "s";
        }
        if($shippingState)
        {
            array_push($tableName,"shipping_state");
            array_push($tableValue,$shippingState);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../profile.php?type=3');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editAddress.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../editAddress.php?type=3');
    }

}
else 
{
    header('Location: ../index.php');
}
?>
