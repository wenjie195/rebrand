<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Puppy.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["pets_uid"]);
    $status = "Yes";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $uid."<br>";
    // echo $status."<br>";

    if(isset($_POST['pets_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"featured_seller");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }    

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $approvedPets = updateDynamicData($conn,"puppy"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($approvedPets)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../featuredPets.php?type=1');

            // $_SESSION['messageType'] = 1;
            // header('Location: ../addFeaturedPuppies.php?type=1');

            header('Location: ' . $_SERVER['HTTP_REFERER']);
            exit;

        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addFeaturedPuppies.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../addFeaturedPuppies.php?type=3');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>
