<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Color.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewColor($conn,$name,$status,$type)
{
     if(insertDynamicData($conn,"color",array("name","status","type"),
          array($name,$status,$type),"sss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $name = rewrite($_POST['register_name']);
    $status = rewrite($_POST['register_status']);
    $type = rewrite($_POST['register_type']);

    $allColor = getColor($conn," WHERE name = ? AND type = ? ",array("name","type"),array($name,$type),"si");
    $existingColor = $allColor[0];

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $name."<br>";
    // echo $status."<br>";

    if(!$existingColor)
    {
        if($name && $status)
        {
            if(registerNewColor($conn,$name,$status,$type))
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../puppyColor.php?type=1');
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../addPuppyColor.php?type=4');
            }
        }
        elseif(!$name)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addPuppyColor.php?type=2');
        }
        elseif(!$status)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addPuppyColor.php?type=3');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../puppyColor.php?type=4');
    }
}
else 
{
     header('Location: ../index.php');
}
?>