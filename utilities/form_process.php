<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Puppy.php';
require_once dirname(__FILE__) . '/../classes/Pets.php';
require_once dirname(__FILE__) . '/../classes/Seller.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

function registerNewPuppy($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$details,$detailsTwo,
          $detailsThree,$detailsFour,$link,$keywordOne,$sellerState,$image0,$image1,$image2,$image3,$image4)
{
     if(insertDynamicData($conn,"puppy",array("uid","name","sku","slug","price","age","vaccinated","dewormed","gender","color","size",
     "status","feature","breed","seller","details","details_two","details_three","details_four","link","keyword_one","location",
     "image_two","image_three","image_four","image_five","image_six"),
          array($uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$details,$detailsTwo,
               $detailsThree,$detailsFour,$link,$keywordOne,$sellerState,$image0,$image1,$image2,$image3,$image4),"sssssssssssssssssssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function registerNewPets($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,$details,$detailsTwo,
          $detailsThree,$detailsFour,$link,$keywordOne,$sellerState,$image0,$image1,$image2,$image3,$image4)
{
     if(insertDynamicData($conn,"pet_details",array("uid","name","sku","slug","price","age","vaccinated","dewormed","gender","color","size","status","feature","breed","seller",
                                   "type","details","details_two","details_three","details_four","link","keyword_one","location",
                                   "image_two","image_three","image_four","image_five","image_six"),
     array($uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,$details,$detailsTwo,
          $detailsThree,$detailsFour,$link,$keywordOne,$sellerState,$image0,$image1,$image2,$image3,$image4),"ssssssssssssssssssssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // die(var_dump($_POST['uploaded_image_name']));
    //Save to DB
    //Do whatever you want

    $conn = connDB();

    $uid = md5(uniqid());

    // $name = rewrite($_POST['register_name']);
    // $name = rewrite($_POST['register_name']);
    $name = " ";

    // original SKU
    // $sku = rewrite($_POST['register_sku']);

    //new SKU
    $registerPetType = "puppy";

    $image0 = "";
    $image1 = "";
    $image2 = "";
    $image3 = "";
    $image4 = "";

    // $sku = $registerPetType.$timestamp;
     // $slug = rewrite($_POST['register_slug']);
     $sku = rewrite($_POST['register_sku']);
     // $slash = "-";
     // $slug = $name.$slash.$sku;
     $slug = " ";

     $price = rewrite($_POST['register_price']);
     $age= rewrite($_POST['register_age']);
     $vaccinated = rewrite($_POST['register_vaccinated']);
     $dewormed = rewrite($_POST['register_dewormed']);
     $gender = rewrite($_POST['register_gender']);
     $color = rewrite($_POST['register_color']);
     $size = rewrite($_POST['register_size']);

     $status = "Available";

     // $feature = rewrite($_POST['register_feature']);
     $feature = " ";

     $breed = rewrite($_POST['register_breed']);

     $sellerName = rewrite($_POST['register_seller']);
     $getSellerDetails = getSeller($conn," WHERE company_name = ? ",array("company_name"),array($sellerName),"s");
     $seller = $getSellerDetails[0]->getUid();
     $sellerState = $getSellerDetails[0]->getState();

     $type = "Puppy";
    $link = rewrite($_POST['register_link']);

    // $keywordOne = rewrite($_POST['register_keyword']);
    $keywordOne = " ";

    $details = rewrite($_POST['register_details']);
    $detailsTwo = rewrite($_POST['register_details_two']);
    $detailsThree = rewrite($_POST['register_details_three']);
    $detailsFour = rewrite($_POST['register_details_four']);

    if(count($_POST['uploaded_image_name']) > 0 && count($_POST['uploaded_image_name']) <= 5){
        //Loop through each file
        for($i=0; $i<count($_POST['uploaded_image_name']); $i++) {
          //Get the temp file path
            $tmpFilePath = $_POST['uploaded_image_name'][$i];

            //Make sure we have a filepath
            if($tmpFilePath != ""){

                //save the filename
                $shortname = $_POST['uploaded_image_name'][$i];

                //save the url and the file
                $filePath = "../upload/" .$_POST['uploaded_image_name'][$i];

                $files[] = $shortname;

                //Upload the file into the temp dir
                if(move_uploaded_file($tmpFilePath, $filePath)) {

                    $files[] = $shortname;
                    //insert into db
                    //use $shortname for the filename
                    //use $filePath for the relative url to the file

                }
              }
        }
    }else {
      echo "Error Image Selected";
    }

    $fileImp = implode(",",$files);
   $fileExp = explode(",",$fileImp);
   for ($i=0; $i <count($fileExp) ; $i++) {
     if ($i == 0) {
       $image0 = $fileExp[$i];
     }if ($i == 1) {
       $image1 = $fileExp[$i];
     }if ($i == 2) {
       $image2 = $fileExp[$i];
     }if ($i == 3) {
       $image3 = $fileExp[$i];
     }if ($i == 4) {
       $image4 = $fileExp[$i];
     }
   }

   // // FOR DEBUGGING 
     // echo "<br>";
     echo $uid."<br>";
     echo $name."<br>";
     echo $sku."<br>";
     echo $slug."<br>";
     echo $price."<br>";
     echo $age."<br>";
     echo $vaccinated ."<br>";
     echo $dewormed."<br>";
     echo $gender."<br>";
     echo $color."<br>";
     echo $size."<br>";
     echo $status."<br>";
     echo $feature."<br>";
     echo $breed."<br>";
     echo $seller."<br>";
     echo $details."<br>";
     echo $link."<br>";
     echo $image0."<br>";
     echo $image1."<br>";
     echo $image2."<br>";
     echo $image3."<br>";
     echo $image4."<br>";

     $puppyNameDetails = getPuppy($conn," WHERE name = ? ",array("name"),array($_POST['register_name']),"s");
     $registeredPuppyName = $puppyNameDetails[0];

     $puppySKUDetails = getPuppy($conn," WHERE sku = ? ",array("sku"),array($_POST['register_sku']),"s");
     $registeredPuppySKU = $puppySKUDetails[0];

     if(!$registeredPuppyName && !$registeredPuppySKU)
     {
          // if(registerNewPuppy($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,
          // $breed,$seller,$details,$link,$imageOne,$imageTwo,$imageThree,$imageFour,$keywordOne,$sellerState))
          if(registerNewPuppy($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$details,$detailsTwo,
               $detailsThree,$detailsFour,$link,$keywordOne,$sellerState,$image0,$image1,$image2,$image3,$image4))
          {
               // if(registerNewPets($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,
               //                          $details,$link,$imageOne,$imageTwo,$imageThree,$imageFour,$keywordOne,$sellerState))
               if(registerNewPets($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,$details,$detailsTwo,
                    $detailsThree,$detailsFour,$link,$keywordOne,$sellerState,$image0,$image1,$image2,$image3,$image4))
               {
                    $_SESSION['newpets_uid'] = $uid;
                    $_SESSION['newpets_type'] = $type;
                    header('Location: ../cropPetsImageP1.php');
               }
               else
               { 
                    $_SESSION['messageType'] = 1;
                    header('Location: ../addPuppy.php?type=2');
               } 
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addPuppy.php?type=3');
          }  
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../addPuppy.php?type=4');
     }     
}
else 
{
     header('Location: ../index.php');
}
?>