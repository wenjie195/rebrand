<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Services.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewService($conn,$serviceName,$status,$type)
{
     if(insertDynamicData($conn,"services",array("service","status","type"),array($serviceName,$status,$type),"ssi") === null)
    {
        echo "gg";
        // header('Location: ../addReferee.php?promptError=1');
        //     promptError("error registering new account.The account already exist");
        //     return false;
    }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $serviceName = rewrite($_POST['register_name']);
    $status = "Available";
    $type = "1";

    $allServices = getServices($conn," WHERE service = ? AND type = ? ",array("service","type"),array($serviceName,$type),"si");
    $existingServices = $allServices[0];

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $serviceName."<br>";

    if(!$existingServices)
    {
        if(registerNewService($conn,$serviceName,$status,$type))
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../partnerServices.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addPartnerService.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../addPartnerService.php?type=3');
    }

}
else 
{
     header('Location: ../index.php');
}

?>