<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Breed.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $id = rewrite($_POST["breed_id"]);
    $status = "Delete";
    $type = "10";

    $previousType = rewrite($_POST["breed_type"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $breed = getBreed($conn," id = ?   ",array("id"),array($id),"s");    

    if(!$breed)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($type)
        {
            array_push($tableName,"type");
            array_push($tableValue,$type);
            $stringType .=  "i";
        }

        array_push($tableValue,$id);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"breed"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 2;
            header('Location: ../puppyBreed.php?type=1');

            // if($previousType == '1')
            // {
            //     $_SESSION['messageType'] = 2;
            //     header('Location: ../puppyBreed.php?type=1');
            // }
            // elseif($previousType == '2')
            // {
            //     $_SESSION['messageType'] = 2;
            //     header('Location: ../kittenBreed.php?type=1');
            // }
            // elseif($previousType == '3')
            // {
            //     $_SESSION['messageType'] = 2;
            //     header('Location: ../reptileBreed.php?type=1');
            // }

        }
        else
        {
            $_SESSION['messageType'] = 2;
            header('Location: ../puppyBreed.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 2;
        header('Location: ../puppyBreed.php?type=3');
    }

}
else 
{
    header('Location: ../index.php');
}
?>
