-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2021 at 10:11 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mypetslib2`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `seo_title` text DEFAULT NULL,
  `article_link` text DEFAULT NULL,
  `keyword_one` text DEFAULT NULL,
  `keyword_two` text DEFAULT NULL,
  `title_cover` text DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `img_cover_source` text DEFAULT NULL,
  `img_one_source` text DEFAULT NULL,
  `img_two_source` text DEFAULT NULL,
  `img_three_source` text DEFAULT NULL,
  `img_four_source` text DEFAULT NULL,
  `img_five_source` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `uid`, `author_uid`, `author_name`, `title`, `seo_title`, `article_link`, `keyword_one`, `keyword_two`, `title_cover`, `paragraph_one`, `image_one`, `paragraph_two`, `image_two`, `paragraph_three`, `image_three`, `paragraph_four`, `image_four`, `paragraph_five`, `image_five`, `img_cover_source`, `img_one_source`, `img_two_source`, `img_three_source`, `img_four_source`, `img_five_source`, `author`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, '9892e3e88f07ccbd84cff19b8a99751e', 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'Think Thrice Before Getting A Pet', 'ThinkThriceBeforeGettingAPet-think-thrice-before-getting-a-pet', 'think-thrice-before-getting-a-pet', ' Are you googling Puppy For Sale, Puppy Seller or Cat or Dog Breeder to get yourself a puppy or kitten as pet right now? One moment please, why do you want to get a pet at the first place? Because they are fluffy, cuddly and cute? Well, we can’t deny that', 'Puppy For Sale,Puppy Seller,Cat or Dog Breeder,toy poodle puppy,', '9892e3e88f07ccbd84cff19b8a99751eblog-picture.jpg', '<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">Are you googling &ldquo;</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Puppy For Sale</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&rdquo;, &ldquo;</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Puppy Seller</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&rdquo; or &ldquo;</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Cat or Dog Breeder</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&rdquo; to get yourself a puppy or kitten as pet right now? One moment please, why do you want to get a pet at the first place? Because they are fluffy, cuddly and cute? Well, we can&rsquo;t deny that this is normally what we thought as first reason as who can resist a </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>toy poodle puppy</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> that resembles a tiny teddy bear!</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh3.googleusercontent.com/ZNeG7pYfhQLKnh3XqURvYiDm36BO_lemAqauO3Hq12MbcH1slc2hUqIKXN6uwteghzo3s2xAFGxpSCKkybKf6Mglepx6kWEIQRptp6suJWXtdAyunyc1OZ1w67B6UFc799xXtdM1\" style=\"height:531px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">If that&rsquo;s your only reason to own a pet because they are adorable at puppy or kitten stage, do think twice because you need to accept the fact that they will eventually grow up and become mature. Despite your lovely puppy and kitten growing up healthily is a good thing, but some people just feeling &ldquo;disappointed&rdquo; that their pet is no longer the mini fluffy ball like they used to be, the expenses doubled because they eat more and requires regular grooming etc. This is the stage where some of these &ldquo;disappointed&rdquo; owners intend to find a new home for their pet.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/uPgP4OHuMNIYmV1t_B0UPsB_-z5d9TcjC2BssuXLiY48fty_3CqE1GW95Aqx3nQn1NKO0aQYmd_XZQybVg72jEHeSxPkkpB56e_M1Jou_a1jqvgDKkbVZWTWW0dEfSUBH9Bgjt_B\" style=\"height:465px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">&nbsp;Before you look for </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><strong>dog breeders</strong></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"> or hanging around </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><strong>pet shops in Malaysia</strong></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">, do think thrice can you handle the responsibility of taking care of your pet for the rest of his life?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">1)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Do you have the patience to train them to pee at the right place without being frustrated?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">2)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Do you have the time to give them some precious attention and a short walk everyday?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">3)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Are you willing to spend on his food and medical fees?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">4)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Do your family members have any objections in getting a pet?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">5)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Most importantly, whether you can ACCEPT THAT THEY WILL GROW UP EVENTUALLY?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh3.googleusercontent.com/QCWDTNDhp7jYedKEnwxbPRJf8qYWtrrv84zKXtPVCxhiI4NolgvGx2QYrGJRv329BkfgRz7ySUTiJz0f9vMNtIgqGFk5Et3vSm0GYQ536JWZXJzqoR4AtIARBnT2b8o1UK54sCIa\" style=\"height:352px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&nbsp;If you are all set and can carry these huge responsibilities, as well as handling grown-up version of your pet, then CONGRATULATIONS, you are ready for a puppy or kitten!</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh3.googleusercontent.com/bR0O2pBc7-qlSQEVzJKMwpHmbEClWm8l8cus534hdRT7qoDuAp4DA1_B-8HDpmZrqAfYcFzyye4wZEfswOKRBdiX4Fh0WSMiz8gmBwfKdVsuDrJM0vrv9WR5ZdTRXDoUUb6enbWq\" style=\"height:351px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">Now, here&rsquo;s the tough part, where to look for reputable </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>kitten and puppy seller</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">, where to search for your preferred breed? For example, </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>British Shorthair Kitten, Persian Kitten, Frenchie Puppy, Corgi Puppy, Toy Poodle Puppy </strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">and others etc. Are you looking for the one that really caught your attention and fall in love with him at the first sight? Wish you have more options? Or you are afraid of dealing with scammers who disappeared after receiving your deposit? Are you worried that the pets you brought home could be problematic and having health issues?&nbsp; Put these worries away, because&hellip;&hellip;.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/KipEBPcGjCKhWy4WvTK4h3-YzxcE1aO4SWN87sOHwU_OfWivL7quajv12_WBtV5-hgF8474olC3-CqVdEGEBQebUbF_vrtjHp10bnj6f6RGHvyQ_sBFQ8K1jZl3p8XWaBz_PW55j\" style=\"height:401px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">TA-DAA! The pawsome platform is here &ndash; MYPETSLIBRARY.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh6.googleusercontent.com/ZfSeAHb6tPg_HxU0zIPj9q07XQh2uxdvHhvce2vLaG_frVHxtp9n7Ro6LxWPtgPFoiKY9XiMD-vCsy0PAJzs2aPKuafLyX_l592HWqbNiBqC9AQBdp_6LFAJOtKMI_rRf3ExylAU\" style=\"height:235px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">Mypetslibrary is the 1</span></span></span></span><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">st</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> Online Pet Store Platform featuring pets and gathers trusted </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>pet sellers</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> across Malaysia. In Mypetslibrary, they offer variety breeds of </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>kittens and puppies for sale </strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">by these </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>pet sellers</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> and </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>pet shops</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">. Rest assured, they are well known and have up to 5 years experiences. You can simply browse for your favourite </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Pomeranian puppy</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> in </span></span></span></span><a href=\"http://www.mypetslibrary.com/\" style=\"text-decoration:none\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#2e6f63\"><span style=\"background-color:#ffffff\">www.mypetslibrary.com</span></span></span></span></a><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">, scrolling at their full set of pictures and videos, and contact the seller instantly if you fall in love with the </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Pomeranian puppy</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> at first glance and decided to give him a forever loving home. Hence, if you browse for your new pet in Mypetslibrary, there will no worries of dealing with irresponsible sellers, scammers or having limited choice of your favourite breed.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/PwDLiTHQ-pjMAjdr_467rH7x9W9k3sCsgzppYkRCejsOHvWSnSP72q8NzcLpmpt6lopFdtjnixhAOx1NbAudoFoNWiRTXrN3AVY8nUnXaiQbXllTQdMIYsGsRHnolsnY03j6_tcQ\" style=\"height:340px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">If you are not sure on which type of breed suits your lifestyle and home environment, or you are a new paw daddy &amp; mummy, and in need of some advice on how to handle your new family member, you may look for Mypetslibrary&rsquo;s customer service helpline, say HELLO and let&rsquo;s have some Pawtalk. They will answer your inquiries well for sure.&nbsp;</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In general, looking after a pet may be a big responsibility, but when you consider about their lovely companionship, how they never let you being alone, the fun and laughter they brought to your family, these pets will make all that hard work of yours worthwhile. </span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/qD0SOc1YpGWZx9kT37lU_BqiJCDUHKOPy6GVeQ7T5Q_oeNoATi61AUL3vqUr-dgOM934rQIsEImuj5gGgR84wLJWh96aCbeSuU1Ay2QJtdHkaMPQezE20lXd1D-bxPxsvW96DVUM\" style=\"height:172px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh5.googleusercontent.com/GS1fK8WRdJ9wge_GzmdW1TGunhbH38z5EXZCSlm1UTCw1cUAvUyL55Hcl5RPF5hSBktEqp184E6p2rnVbi5EX-rBy53PCUu3DmP0WvLfpGLxxoVBDbm2e81b7yIGdzDG0h4BPPxT\" style=\"height:409px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-05-12 03:22:25', '2020-07-02 08:31:27'),
(2, '2250064b0ccfe80d005eaac65d3058cd', 'd156938c9b63fd253e9cddf89eba10e7', 'admin', '养宠物前先三思', '养宠物前先三思-养宠物前先三思', '养宠物前先三思', '您是否正在搜寻待售的幼犬，幼犬卖家或猫或狗饲养员，以让自己成为宠物的幼犬或小猫？请等一下，为什么首先要养宠物？是因为它们蓬松，可爱又可爱吗？好吧，我们不能否认', '出售幼犬，幼犬卖方，猫或狗饲养员，玩具贵宾犬幼犬', '2250064b0ccfe80d005eaac65d3058cd9892e3e88f07ccbd84cff19b8a99751eblog-picture.jpg', '<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\"><span style=\"font-family:Times New Roman,Times,serif\">您是否正在搜寻&ldquo;待售幼犬&rdquo;，&ldquo;幼犬卖家&rdquo;或&ldquo;猫或狗饲养员&rdquo;，以让自己成为宠物的幼犬或小猫？请等一下，为什么首先要养宠物？是因为它们蓬松，可爱又可爱吗？好吧，我们不能否认这通常是我们认为的第一个理由，因为谁能抵抗像玩具熊一样的玩具贵宾犬小狗！</span></span></p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"font-family:Times New Roman,Times,serif\"><img src=\"https://lh3.googleusercontent.com/ZNeG7pYfhQLKnh3XqURvYiDm36BO_lemAqauO3Hq12MbcH1slc2hUqIKXN6uwteghzo3s2xAFGxpSCKkybKf6Mglepx6kWEIQRptp6suJWXtdAyunyc1OZ1w67B6UFc799xXtdM1\" style=\"height:531px; width:624px\" /></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\">如果这是您养宠物的唯一原因，因为它们在幼犬或小猫阶段很可爱，请三思而后行，因为您需要接受一个事实，即它们最终会长大并成熟。尽管您可爱的小狗和小猫健康成长是一件好事，但有些人只是感到&ldquo;失望&rdquo;，因为他们的宠物不再像以前那样是迷你蓬松球了，费用翻了一番，因为他们吃得更多并且需要定期梳理等等。在此阶段，其中一些&ldquo;失望的&rdquo;主人打算为他们的宠物寻找新家。</span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img src=\"https://lh4.googleusercontent.com/uPgP4OHuMNIYmV1t_B0UPsB_-z5d9TcjC2BssuXLiY48fty_3CqE1GW95Aqx3nQn1NKO0aQYmd_XZQybVg72jEHeSxPkkpB56e_M1Jou_a1jqvgDKkbVZWTWW0dEfSUBH9Bgjt_B\" /></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:16px\"><span style=\"font-family:Arial,Helvetica,sans-serif\">在马来西亚寻找养狗人或在宠物商店附近闲逛之前，您认为三次可以承担照顾宠物一生的责任吗？</span></span></p>\r\n\r\n<p style=\"text-align:center\">1）您是否有耐心训练他们在正确的地方撒尿而不会感到沮丧？</p>\r\n\r\n<p style=\"text-align:center\">2）你每天有时间给他们一些宝贵的关注和短暂的步行吗？</p>\r\n\r\n<p style=\"text-align:center\">3）你愿意花他的食物和医疗费用吗？</p>\r\n\r\n<p style=\"text-align:center\">4）您的家人对养宠物有异议吗？</p>\r\n\r\n<p style=\"text-align:center\">5）最重要的是，您是否可以接受他们将长大成人？</p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-08-21 03:07:55', '2020-08-21 03:15:08');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Royal Canin', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(2, 'Brit', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(3, 'Hills', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(4, 'EUKANUBA', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(5, 'Iskhan', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(6, 'VitalPlus', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(7, 'Monge', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(8, 'ProBalance', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(9, 'Happy Dog', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(10, 'Nature Protection', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(11, 'Back2Nature', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(12, 'SmartHeart', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(13, 'FRISKIES', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(14, 'Whiskas', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(15, 'ProDiet', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(16, 'IAMS', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(17, 'Power Cat', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17'),
(18, 'Misha', 'Available', '2021-02-01 05:37:17', '2021-02-01 05:37:17');

-- --------------------------------------------------------

--
-- Table structure for table `breed`
--

CREATE TABLE `breed` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `breed`
--

INSERT INTO `breed` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(9, 'Lizard', 'Available', 3, '2020-04-21 09:25:51', '2020-04-21 09:25:51'),
(10, 'Frog', 'Available', 3, '2020-04-21 09:28:28', '2020-04-21 09:28:28'),
(11, 'Akita', 'Available', 1, '2020-06-24 04:10:25', '2020-06-24 04:10:25'),
(12, 'Alaskan Husky', 'Available', 1, '2020-06-24 04:10:35', '2020-06-24 04:10:35'),
(13, 'Alaskan Malamute', 'Available', 1, '2020-06-24 04:11:03', '2020-06-24 04:11:03'),
(14, 'American Bully', 'Available', 1, '2020-06-24 04:11:25', '2020-06-24 04:11:25'),
(15, 'Bassett Hound', 'Available', 1, '2020-06-24 04:11:31', '2020-06-24 04:11:31'),
(16, 'Beagle', 'Available', 1, '2020-06-24 04:11:37', '2020-06-24 04:11:37'),
(17, 'Bernese Mountain Dog', 'Available', 1, '2020-06-24 04:11:45', '2020-06-24 04:11:45'),
(18, 'Bichon Frise', 'Available', 1, '2020-06-24 04:11:50', '2020-06-24 04:11:50'),
(19, 'Border Collie', 'Available', 1, '2020-06-24 04:11:56', '2020-06-24 04:11:56'),
(20, 'Bull Terrier', 'Available', 1, '2020-06-24 04:12:02', '2020-06-24 04:12:02'),
(21, 'Bulldog', 'Available', 1, '2020-06-24 04:12:07', '2020-06-24 04:12:07'),
(22, 'Cane Corso', 'Available', 1, '2020-06-24 04:12:14', '2020-06-24 04:12:14'),
(23, 'Caucasian Shepherd', 'Available', 1, '2020-06-24 04:12:21', '2020-06-24 04:12:21'),
(24, 'Cavalier King Charles Spaniel', 'Available', 1, '2020-06-24 04:12:29', '2020-06-24 04:12:29'),
(25, 'Chihuahua', 'Available', 1, '2020-06-24 04:12:37', '2020-06-24 04:12:37'),
(26, 'Chow Chow', 'Available', 1, '2020-06-24 04:12:42', '2020-06-24 04:12:42'),
(27, 'Cocker Spaniel', 'Available', 1, '2020-06-24 04:12:48', '2020-06-24 04:12:48'),
(28, 'Dachshund', 'Available', 1, '2020-06-24 04:12:55', '2020-06-24 04:12:55'),
(29, 'Dalmatian', 'Available', 1, '2020-06-24 04:13:01', '2020-06-24 04:13:01'),
(30, 'Dobermann', 'Available', 1, '2020-06-24 04:13:09', '2020-06-24 04:13:09'),
(31, 'English Bulldog', 'Available', 1, '2020-06-24 04:13:16', '2020-06-24 04:13:16'),
(32, 'French Bulldog', 'Available', 1, '2020-06-24 04:13:35', '2020-06-24 04:13:35'),
(33, 'German Shepherd', 'Available', 1, '2020-06-24 04:13:42', '2020-06-24 04:13:42'),
(34, 'Giant Poodle', 'Available', 1, '2020-06-24 04:13:50', '2020-06-24 04:13:50'),
(35, 'Golden Retriever', 'Available', 1, '2020-06-24 04:13:56', '2020-06-24 04:13:56'),
(36, 'Great Dane', 'Available', 1, '2020-06-24 04:14:01', '2020-06-24 04:14:01'),
(37, 'Jack Russell', 'Available', 1, '2020-06-24 04:14:07', '2020-06-24 04:14:07'),
(38, 'Labrador', 'Available', 1, '2020-06-24 04:14:13', '2020-06-24 04:14:13'),
(39, 'Maltese', 'Available', 1, '2020-06-24 04:14:18', '2020-06-24 04:14:18'),
(40, 'Maltipoo', 'Available', 1, '2020-06-24 04:14:28', '2020-06-24 04:14:28'),
(41, 'Miniature poodle', 'Available', 1, '2020-06-24 04:14:34', '2020-06-24 04:14:34'),
(42, 'Mix Breed', 'Available', 1, '2020-06-24 04:14:40', '2020-06-24 04:14:40'),
(43, 'Morkie', 'Available', 1, '2020-06-24 04:14:44', '2020-06-24 04:14:44'),
(44, 'Old English Sheepdog', 'Available', 1, '2020-06-24 04:14:50', '2020-06-24 04:14:50'),
(45, 'Papillon', 'Available', 1, '2020-06-24 04:14:55', '2020-06-24 04:14:55'),
(46, 'Pekingese', 'Available', 1, '2020-06-24 04:15:00', '2020-06-24 04:15:00'),
(47, 'Pinscher', 'Available', 1, '2020-06-24 04:15:08', '2020-06-24 04:15:08'),
(48, 'Pitbull', 'Available', 1, '2020-06-24 04:15:14', '2020-06-24 04:15:14'),
(49, 'Pomapoo', 'Available', 1, '2020-06-24 04:15:20', '2020-06-24 04:15:20'),
(50, 'Pomeranian', 'Available', 1, '2020-06-24 04:15:27', '2020-06-24 04:15:27'),
(51, 'Pomsky', 'Available', 1, '2020-06-24 04:15:32', '2020-06-24 04:15:32'),
(52, 'Pug', 'Available', 1, '2020-06-24 04:15:37', '2020-06-24 04:15:37'),
(53, 'Rottweiler', 'Available', 1, '2020-06-24 04:15:42', '2020-06-24 04:15:42'),
(54, 'Saint Bernard', 'Available', 1, '2020-06-24 04:15:47', '2020-06-24 04:15:47'),
(55, 'Samoyed', 'Available', 1, '2020-06-24 04:15:52', '2020-06-24 04:15:52'),
(56, 'Schnauzer', 'Available', 1, '2020-06-24 04:15:59', '2020-06-24 04:15:59'),
(57, 'Shar Pei', 'Available', 1, '2020-06-24 04:16:05', '2020-06-24 04:16:05'),
(58, 'Shetland Sheepdog', 'Available', 1, '2020-06-24 04:16:11', '2020-06-24 04:16:11'),
(59, 'Shiba Inu', 'Available', 1, '2020-06-24 04:16:18', '2020-06-24 04:16:18'),
(60, 'Shih Tzu', 'Available', 1, '2020-06-24 04:16:24', '2020-06-24 04:16:24'),
(61, 'Siberian Husky', 'Available', 1, '2020-06-24 04:16:30', '2020-06-24 04:16:30'),
(62, 'Silky Terrier', 'Available', 1, '2020-06-24 04:16:37', '2020-06-24 04:16:37'),
(63, 'Standard poodle', 'Available', 1, '2020-06-24 04:16:43', '2020-06-24 04:16:43'),
(64, 'Teacup Pomeranian', 'Available', 1, '2020-06-24 04:16:49', '2020-06-24 04:16:49'),
(65, 'Teacup poodle', 'Available', 1, '2020-06-24 04:16:54', '2020-06-24 04:16:54'),
(66, 'Tiny Poodle', 'Available', 1, '2020-06-24 04:16:59', '2020-06-24 04:16:59'),
(67, 'Toy poodle', 'Available', 1, '2020-06-24 04:17:05', '2020-06-24 04:17:05'),
(68, 'Welsh Corgi', 'Available', 1, '2020-06-24 04:17:43', '2020-06-24 04:17:43'),
(69, 'West Highland Terrier', 'Available', 1, '2020-06-24 04:17:49', '2020-06-24 04:17:49'),
(70, 'Wooly Husky', 'Available', 1, '2020-06-24 04:17:56', '2020-06-24 04:17:56'),
(71, 'Wooly Malamute', 'Available', 1, '2020-06-24 04:18:02', '2020-06-24 04:18:02'),
(72, 'Yorkshire Terrier', 'Available', 1, '2020-06-24 04:18:09', '2020-06-24 04:18:09'),
(73, 'American Curl', 'Available', 2, '2020-06-24 04:19:47', '2020-06-24 04:19:47'),
(74, 'American Shorthair', 'Available', 2, '2020-06-24 04:19:52', '2020-06-24 04:19:52'),
(75, 'Bengal', 'Available', 2, '2020-06-24 04:19:58', '2020-06-24 04:19:58'),
(76, 'British Longhair', 'Available', 2, '2020-06-24 04:20:03', '2020-06-24 04:20:03'),
(77, 'British Shorthair', 'Available', 2, '2020-06-24 04:20:09', '2020-06-24 04:20:09'),
(78, 'Domestic Longhair', 'Available', 2, '2020-06-24 04:20:14', '2020-06-24 04:20:14'),
(79, 'Domestic Shorthair', 'Available', 2, '2020-06-24 04:20:20', '2020-06-24 04:20:20'),
(80, 'Exotic Shorthair', 'Available', 2, '2020-06-24 04:20:29', '2020-06-24 04:20:29'),
(81, 'Himalayan', 'Available', 2, '2020-06-24 04:20:33', '2020-06-24 04:20:33'),
(82, 'Mainecoon', 'Available', 2, '2020-06-24 04:20:40', '2020-06-24 04:20:40'),
(83, 'Minuet', 'Available', 2, '2020-06-24 04:20:45', '2020-06-24 04:20:45'),
(84, 'Mix Breed', 'Available', 2, '2020-06-24 04:20:51', '2020-06-24 04:20:51'),
(85, 'Munchkin', 'Available', 2, '2020-06-24 04:20:56', '2020-06-24 04:20:56'),
(86, 'Persian', 'Available', 2, '2020-06-24 04:21:01', '2020-06-24 04:21:01'),
(87, 'Ragdoll', 'Available', 2, '2020-06-24 04:21:06', '2020-06-24 04:21:06'),
(88, 'Scottishfold', 'Available', 2, '2020-06-24 04:21:12', '2020-06-24 04:21:12'),
(89, 'Scottishstraight', 'Available', 2, '2020-06-24 04:21:19', '2020-06-24 04:21:19'),
(90, 'Sphynx', 'Available', 2, '2020-06-24 04:21:24', '2020-06-24 04:21:24');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Dog Dry Food', 'Available', '2021-01-04 10:52:36', '2021-01-04 10:52:36'),
(2, 'Dog Wet Food', 'Available', '2021-01-04 10:52:44', '2021-01-04 10:52:44'),
(3, 'Dog Treat / Snack', 'Available', '2021-01-04 10:52:53', '2021-01-04 10:52:53'),
(4, 'Dog Toys and Accessory', 'Available', '2021-01-04 10:53:04', '2021-01-04 10:53:04'),
(5, 'Cat Dry Food', 'Available', '2021-01-04 10:53:41', '2021-01-04 10:53:41'),
(6, 'Cat Wet Food', 'Available', '2021-01-04 10:53:47', '2021-01-04 10:53:47'),
(7, 'Cat Treat / Snack', 'Available', '2021-01-04 10:53:52', '2021-01-04 10:53:52'),
(8, 'Cat Toys and Accessory', 'Available', '2021-01-04 10:54:13', '2021-01-04 10:54:13'),
(9, 'Dog Healthcare', 'Available', '2021-01-29 11:14:24', '2021-01-29 11:14:24'),
(10, 'Cat Healthcare', 'Available', '2021-01-29 11:15:30', '2021-01-29 11:15:30');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(4, 'Grey', 'Available', 2, '2020-04-21 08:05:51', '2020-04-21 08:05:51'),
(5, 'White', 'Available', 2, '2020-04-21 09:02:02', '2020-04-21 09:02:02'),
(6, 'Orange', 'Available', 2, '2020-04-21 09:13:20', '2020-04-21 09:13:20'),
(7, 'Black', 'Available', 3, '2020-04-21 09:15:13', '2020-04-21 09:15:13'),
(8, 'Red', 'Available', 2, '2020-04-21 09:19:46', '2020-04-21 09:19:46'),
(9, 'Green', 'Available', 3, '2020-04-21 09:32:48', '2020-04-21 09:32:48'),
(10, 'orange', 'Available', 3, '2020-05-27 07:11:39', '2020-05-27 07:11:39'),
(11, 'Apricot', 'Available', 1, '2020-07-01 08:14:32', '2020-07-01 08:14:32'),
(12, 'Black', 'Available', 1, '2020-07-01 08:14:44', '2020-07-01 08:14:44'),
(13, 'Black &amp; Tan', 'Available', 1, '2020-07-01 08:14:55', '2020-07-01 08:14:55'),
(14, 'Black &amp; White', 'Available', 1, '2020-07-01 08:15:04', '2020-07-01 08:15:04'),
(15, 'Blue Merle', 'Available', 1, '2020-07-01 08:15:13', '2020-07-01 08:15:13'),
(16, 'Brown', 'Available', 1, '2020-07-01 08:15:21', '2020-07-01 08:15:21'),
(17, 'Copper &amp; White', 'Available', 1, '2020-07-01 08:15:29', '2020-07-01 08:15:29'),
(18, 'Cream', 'Available', 1, '2020-07-01 08:15:38', '2020-07-01 08:15:38'),
(19, 'Cream White', 'Available', 1, '2020-07-01 08:15:45', '2020-07-01 08:15:45'),
(20, 'Fawn', 'Available', 1, '2020-07-01 08:15:52', '2020-07-01 08:15:52'),
(21, 'Fawn &amp; White', 'Available', 1, '2020-07-01 08:16:12', '2020-07-01 08:16:12'),
(22, 'Gold', 'Available', 1, '2020-07-01 08:16:20', '2020-07-01 08:16:20'),
(23, 'Latte', 'Available', 1, '2020-07-01 08:16:26', '2020-07-01 08:16:26'),
(24, 'Light Brown', 'Available', 1, '2020-07-01 08:16:32', '2020-07-01 08:16:32'),
(25, 'Liver &amp; Tan', 'Available', 1, '2020-07-01 08:16:38', '2020-07-01 08:16:38'),
(26, 'Orange', 'Available', 1, '2020-07-01 08:16:46', '2020-07-01 08:16:46'),
(27, 'Parti', 'Available', 1, '2020-07-01 08:16:53', '2020-07-01 08:16:53'),
(28, 'Red/Copper', 'Available', 1, '2020-07-01 08:17:00', '2020-07-01 08:17:00'),
(29, 'Red Merle', 'Available', 1, '2020-07-01 08:17:26', '2020-07-01 08:17:26'),
(30, 'Sable Brown', 'Available', 1, '2020-07-01 08:17:39', '2020-07-01 08:17:39'),
(31, 'Sable Gray', 'Available', 1, '2020-07-01 08:17:46', '2020-07-01 08:17:46'),
(32, 'Sable Orange', 'Available', 1, '2020-07-01 08:17:52', '2020-07-01 08:17:52'),
(33, 'Salt &amp; Pepper', 'Available', 1, '2020-07-01 08:17:59', '2020-07-01 08:17:59'),
(34, 'Silver', 'Available', 1, '2020-07-01 08:18:06', '2020-07-01 08:18:06'),
(35, 'Silver &amp; White', 'Available', 1, '2020-07-01 08:18:11', '2020-07-01 08:18:11'),
(36, 'Super Red', 'Available', 1, '2020-07-01 08:18:17', '2020-07-01 08:18:17'),
(37, 'Tri-Colour', 'Available', 1, '2020-07-01 08:18:25', '2020-07-01 08:18:25'),
(38, 'White', 'Available', 1, '2020-07-01 08:18:32', '2020-07-01 08:18:32');

-- --------------------------------------------------------

--
-- Table structure for table `credit_card`
--

CREATE TABLE `credit_card` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` int(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `item_uid` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id`, `uid`, `username`, `link`, `item_uid`, `type`, `remark`, `status`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary/malaysia-pets-products-details.php?id=c634208e1ac402adcf6e372882ce99b3', 'c634208e1ac402adcf6e372882ce99b3', 'Product', NULL, 'Yes', '2021-01-06 06:18:53', '2021-01-06 06:18:53'),
(2, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary/malaysia-pets-products-details.php?id=7ba69bf0cc5f01ae0562977fca09ea92', '7ba69bf0cc5f01ae0562977fca09ea92', 'Product', NULL, 'Yes', '2021-01-06 07:46:38', '2021-01-06 07:46:38'),
(3, 'd156938c9b63fd253e9cddf89eba10e7', NULL, '/mypetslibrary/malaysia-pets-products-details.php?id=0f7b32cdabccd5a533bea31c743144ab', '0f7b32cdabccd5a533bea31c743144ab', 'Product', NULL, 'Yes', '2021-01-20 09:13:08', '2021-01-20 09:13:08'),
(4, 'd156938c9b63fd253e9cddf89eba10e7', NULL, '/mypetslibrary/malaysia-pets-products-details.php?id=0f7b32cdabccd5a533bea31c743144ab', '0f7b32cdabccd5a533bea31c743144ab', 'Product', NULL, 'Yes', '2021-01-20 09:13:09', '2021-01-20 09:13:09'),
(5, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary/malaysia-pets-products-details.php?id=de6b3e53200445d688f5797620062c9b', 'de6b3e53200445d688f5797620062c9b', 'Product', NULL, 'Delete', '2021-04-15 14:15:20', '2021-04-15 14:15:22');

-- --------------------------------------------------------

--
-- Table structure for table `fb_login`
--

CREATE TABLE `fb_login` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'facebook_uid',
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Female', 'Available', 1, '2020-05-29 06:33:21', '2020-05-29 06:33:21'),
(2, 'Male', 'Available', 1, '2020-05-29 06:33:21', '2020-05-29 06:33:21');

-- --------------------------------------------------------

--
-- Table structure for table `kitten`
--

CREATE TABLE `kitten` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `message_uid` varchar(255) DEFAULT NULL,
  `receive_message` text DEFAULT NULL,
  `reply_message` text DEFAULT NULL,
  `reply_one` text DEFAULT NULL,
  `reply_two` text DEFAULT NULL,
  `reply_three` text DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, READ = user read sms',
  `admin_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, REPLY = admin reply sms',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `uid`, `username`, `message_uid`, `receive_message`, `reply_message`, `reply_one`, `reply_two`, `reply_three`, `user_status`, `admin_status`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d', 'user', 'ea63477acdd5636914a209d85cd04fcd', 'hi', 'dsfdsfds', 'ea63477acdd5636914a209d85cd04fcd', NULL, 'INABA Twin Packs Recipe Dog Pet Wet Food 80g', 'GET', 'REPLY', '2021-01-06 06:19:16', '2021-01-07 08:05:09'),
(2, 'd6ce9933b57bda792172b01b9aca8a7d', 'test', '2a91f988ea9a4732851958b71f75f91a', 'hello', 'hi', '2a91f988ea9a4732851958b71f75f91a', NULL, 'Parie Goat Milk Powder for Puppies 500gm', 'GET', 'REPLY', '2021-01-07 08:28:17', '2021-01-07 08:30:36'),
(3, 'd6ce9933b57bda792172b01b9aca8a7d', 'test', '12e89a01700bd7d66f4c9a3318dc1763', 'can i know how many you got', NULL, '12e89a01700bd7d66f4c9a3318dc1763', NULL, NULL, 'SENT', 'GET', '2021-01-07 08:30:41', '2021-01-07 08:30:41'),
(4, 'd6ce9933b57bda792172b01b9aca8a7d', NULL, 'ff99ffd9af97bc64cb2305a01021f475', NULL, 'hihi', 'ff99ffd9af97bc64cb2305a01021f475', NULL, NULL, 'GET', 'REPLY', '2021-01-07 08:30:49', '2021-01-07 08:30:49'),
(5, 'd6ce9933b57bda792172b01b9aca8a7d', 'test', 'ae84d8c8a36672eb1107cecd181d63fb', 'aoa alaa', NULL, 'ae84d8c8a36672eb1107cecd181d63fb', NULL, NULL, 'SENT', 'GET', '2021-01-07 08:31:01', '2021-01-07 08:31:01'),
(6, 'd6ce9933b57bda792172b01b9aca8a7d', 'test', '165c35fba253eba9d90d81059bb8f0e0', '123123', NULL, '165c35fba253eba9d90d81059bb8f0e0', NULL, NULL, 'SENT', 'GET', '2021-01-07 08:31:22', '2021-01-07 08:31:22'),
(7, 'd6ce9933b57bda792172b01b9aca8a7d', NULL, '6c8221ffc3d193a8c68bdd74b6bf5bd3', NULL, 'tuioj', '6c8221ffc3d193a8c68bdd74b6bf5bd3', NULL, NULL, 'GET', 'REPLY', '2021-01-07 08:31:33', '2021-01-07 08:31:33'),
(8, 'd6ce9933b57bda792172b01b9aca8a7d', NULL, '2f7f5f982289f41092933925cb64e776', NULL, 'dhjfg', '2f7f5f982289f41092933925cb64e776', NULL, NULL, 'GET', 'REPLY', '2021-01-07 08:31:49', '2021-01-07 08:31:49'),
(9, 'd6ce9933b57bda792172b01b9aca8a7d', NULL, '26b3acce134bbba7fcaf239c2329b083', NULL, 'lpigsohs', '26b3acce134bbba7fcaf239c2329b083', NULL, NULL, 'GET', 'REPLY', '2021-01-07 08:34:30', '2021-01-07 08:34:30'),
(10, 'd6ce9933b57bda792172b01b9aca8a7d', NULL, 'b0d675490db12fd92843969c39ed8f31', NULL, 'hdjdjd', 'b0d675490db12fd92843969c39ed8f31', NULL, NULL, 'GET', 'REPLY', '2021-01-07 08:34:52', '2021-01-07 08:34:52'),
(11, 'd6ce9933b57bda792172b01b9aca8a7d', NULL, 'af534cb92ee277d395a5b10be2d070b2', NULL, 'hjdjd', 'af534cb92ee277d395a5b10be2d070b2', NULL, NULL, 'GET', 'REPLY', '2021-01-07 08:34:56', '2021-01-07 08:34:56'),
(12, 'd6ce9933b57bda792172b01b9aca8a7d', NULL, '6318cc3bb43878b012d7e0c97ca30598', NULL, 'testing', '6318cc3bb43878b012d7e0c97ca30598', NULL, NULL, 'GET', 'REPLY', '2021-01-07 08:35:06', '2021-01-07 08:35:06'),
(13, 'd6ce9933b57bda792172b01b9aca8a7d', 'test', '9b9705e882a206e5cb6c72ca02e86795', 'ggggg', NULL, '9b9705e882a206e5cb6c72ca02e86795', NULL, NULL, 'SENT', 'GET', '2021-01-07 08:35:17', '2021-01-07 08:35:17'),
(14, 'dc0c19ea4deeb8d1e28e9064b6f8d7cf', 'titanlai', 'ab804a63f8abbf65208ff4d857591b6d', 'hello', NULL, 'ab804a63f8abbf65208ff4d857591b6d', NULL, 'Harrington&#039;s Dog Food Complete Turkey And Vegetables Dry Mix 15kg', 'SENT', 'GET', '2021-01-22 09:20:42', '2021-01-22 09:20:42'),
(15, 'dc0c19ea4deeb8d1e28e9064b6f8d7cf', 'titanlai', 'fd688d850baa71cf60e66b9e4b518eb6', 'mama mi ai', NULL, 'fd688d850baa71cf60e66b9e4b518eb6', NULL, NULL, 'SENT', 'GET', '2021-01-22 09:20:49', '2021-01-22 09:20:49'),
(16, '9349aede685bae49c49b0b895e465f6d', 'user', '14f6c5b33ff6b0d0a3f14c806819aa19', 'erwrwrwer', NULL, '14f6c5b33ff6b0d0a3f14c806819aa19', NULL, 'Royal Canin FBN British Short Hair Kitten 2Kg', 'SENT', 'GET', '2021-02-27 08:21:02', '2021-02-27 08:21:02'),
(17, '9349aede685bae49c49b0b895e465f6d', 'user', '1078fcf61c53037e8a800fe759300a97', 'saaa', NULL, '1078fcf61c53037e8a800fe759300a97', NULL, 'Royal Canin FBN British Short Hair Kitten 2Kg', 'SENT', 'GET', '2021-02-27 08:21:47', '2021-02-27 08:21:47');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `address_line_3`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_date`, `payment_time`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `reject_reason`, `refund_method`, `refund_amount`, `refund_note`, `refund_reason`, `receipt`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d1609820390', '9349aede685bae49c49b0b895e465f6d', NULL, NULL, NULL, NULL, 'user', '123321', NULL, '123, Jln User', NULL, NULL, NULL, NULL, NULL, NULL, '16128', NULL, 'BILLPLZ', NULL, 'qdkx0wft', NULL, NULL, 'ACCEPTED', 'DELIVERED', 'Pos Laju', '2021-01-06', '1234', NULL, NULL, NULL, NULL, NULL, '', '2021-01-05 04:20:10', '2021-01-06 08:14:20'),
(2, '9349aede685bae49c49b0b895e465f6d1609820529', '9349aede685bae49c49b0b895e465f6d', NULL, NULL, NULL, NULL, 'user edit', '123332211', NULL, '23, Tmn User 2', NULL, NULL, NULL, NULL, NULL, NULL, '2118', NULL, 'BILLPLZ', NULL, 'fbyktvy5', NULL, NULL, 'ACCEPTED', 'DELIVERED', NULL, '2021-01-07', NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-01-05 04:22:33', '2021-01-07 08:08:48'),
(3, '5d6368155bcc2ec3b4234b250c737ac41609820623', '5d6368155bcc2ec3b4234b250c737ac4', NULL, NULL, NULL, NULL, 'oliver', '01211223301', NULL, '11, Jln Oliver', NULL, NULL, NULL, NULL, NULL, NULL, '11064', NULL, 'BILLPLZ', NULL, 'utxbuz0t', NULL, NULL, 'ACCEPTED', 'DELIVERED', 'ABC', '2021-03-08', '123123', NULL, NULL, NULL, NULL, NULL, '', '2021-01-05 04:23:52', '2021-03-08 10:22:57'),
(4, 'fdefd9112e3579886dfbb9e04d7f928d1609820691', 'fdefd9112e3579886dfbb9e04d7f928d', NULL, NULL, NULL, NULL, 'barry', '01211223302', NULL, '33, Jln Barry', NULL, NULL, NULL, NULL, NULL, NULL, '1670', NULL, 'BILLPLZ', NULL, 'p9zo2xbs', NULL, NULL, 'ACCEPTED', 'DELIVERED', 'XXX', '2021-04-08', 'ABC123', NULL, NULL, NULL, NULL, NULL, '', '2021-01-05 04:25:01', '2021-04-07 16:11:55'),
(5, 'fdefd9112e3579886dfbb9e04d7f928d1609820733', 'fdefd9112e3579886dfbb9e04d7f928d', NULL, NULL, NULL, NULL, 'barry', '01211223302', NULL, '23, Tmn Barry', NULL, NULL, NULL, NULL, NULL, NULL, '4288', NULL, 'BILLPLZ', NULL, NULL, NULL, NULL, 'WAITING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-01-05 04:25:52', '2021-01-05 04:25:52'),
(6, '9349aede685bae49c49b0b895e465f6d1609918687', '9349aede685bae49c49b0b895e465f6d', NULL, NULL, NULL, NULL, 'user', '123321', NULL, '123,asd asd, asd zxc, 123321, Johor.', NULL, NULL, NULL, NULL, NULL, NULL, '76', NULL, 'BILLPLZ', NULL, 'cxvzdnhc', NULL, NULL, 'PENDING', 'TBC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-01-06 07:38:12', '2021-01-06 07:38:17'),
(7, '9349aede685bae49c49b0b895e465f6d1609918776', '9349aede685bae49c49b0b895e465f6d', NULL, NULL, NULL, NULL, 'user', '123321', NULL, '123,asd asd, asd zxc, 123321, Johor.', NULL, NULL, NULL, NULL, NULL, NULL, '70', NULL, 'BILLPLZ', NULL, 'ih8xenr8', NULL, NULL, 'PENDING', 'TBC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-01-06 07:39:45', '2021-01-06 07:39:53'),
(8, '9349aede685bae49c49b0b895e465f6d1609919207', '9349aede685bae49c49b0b895e465f6d', NULL, NULL, NULL, NULL, 'user', '123321', NULL, '123,asd asd, asd zxc, 123321, Johor.', NULL, NULL, NULL, NULL, NULL, NULL, '70', NULL, 'BILLPLZ', NULL, '3gw1kfe0', NULL, NULL, 'PENDING', 'TBC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-01-06 07:46:52', '2021-01-06 07:47:02'),
(9, '5633b78c7f79f1dfc93870d84c00abb31610086004', '5633b78c7f79f1dfc93870d84c00abb3', NULL, NULL, NULL, NULL, 'micht', '0164646483', NULL, 'No 27,, shah alam, 41050, Kuala Lumpur.', NULL, NULL, NULL, NULL, NULL, NULL, '70', NULL, 'BILLPLZ', NULL, 'svorwj6y', NULL, NULL, 'PENDING', 'TBC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-01-08 06:07:02', '2021-01-08 06:07:08'),
(10, '97af0bfb9c1f8ca19db23ac31139d6bc1611070525', '97af0bfb9c1f8ca19db23ac31139d6bc', NULL, NULL, NULL, NULL, 'leon', '01888888888', NULL, 'B-12-12, Petaling Jaya, 47820, Selangor.', NULL, NULL, NULL, NULL, NULL, NULL, '200', NULL, 'BILLPLZ', NULL, '6bjbrqdt', NULL, NULL, 'PENDING', 'TBC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-01-19 15:35:49', '2021-01-19 15:35:55'),
(11, 'd156938c9b63fd253e9cddf89eba10e71611134052', 'd156938c9b63fd253e9cddf89eba10e7', NULL, NULL, NULL, NULL, 'admin', '123321', NULL, 'asdasdasd, asdasdsad, 12312321, Select State.', NULL, NULL, NULL, NULL, NULL, NULL, '80', NULL, 'BILLPLZ', NULL, 'mpgbfrxg', NULL, NULL, 'PENDING', 'TBC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-01-20 09:14:44', '2021-01-20 09:15:08'),
(12, 'dc0c19ea4deeb8d1e28e9064b6f8d7cf1611307281', 'dc0c19ea4deeb8d1e28e9064b6f8d7cf', NULL, NULL, NULL, NULL, 'titanlai', '0122664807', NULL, 'asdsadsad, 1231, 1223123, Labuan.', NULL, NULL, NULL, NULL, NULL, NULL, '300', NULL, 'BILLPLZ', NULL, 'mtrwrd5b', NULL, NULL, 'PENDING', 'TBC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-01-22 09:21:29', '2021-01-22 09:21:37'),
(13, '9349aede685bae49c49b0b895e465f6d1612173519', '9349aede685bae49c49b0b895e465f6d', NULL, NULL, NULL, NULL, 'user', '123321', NULL, '123, Jalan Durian, Kampung Rambutan,, Taiping, 34000, Johor.', NULL, NULL, NULL, NULL, NULL, NULL, '6', NULL, 'BILLPLZ', NULL, 'wyd0sxsi', NULL, NULL, 'ACCEPTED', 'DELIVERED', 'Pos Laju', '2021-02-01', '1234', NULL, NULL, NULL, NULL, NULL, '', '2021-02-01 09:58:44', '2021-02-01 09:59:44'),
(14, '9349aede685bae49c49b0b895e465f6d1612173624', '9349aede685bae49c49b0b895e465f6d', NULL, NULL, NULL, NULL, 'user', '123321', NULL, '123, Jalan Durian, Kampung Rambutan,, Taiping, 34000, Johor.', NULL, NULL, NULL, NULL, NULL, NULL, '70', NULL, 'BILLPLZ', NULL, 'lmhkhew3', NULL, NULL, 'PENDING', 'TBC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-02-01 10:00:28', '2021-02-01 10:00:33'),
(15, '9349aede685bae49c49b0b895e465f6d1615189380', '9349aede685bae49c49b0b895e465f6d', NULL, NULL, NULL, NULL, 'user', '123321', NULL, '123, Jalan Durian, Kampung Rambutan,, Taiping, 34000, Johor.', NULL, NULL, NULL, NULL, NULL, NULL, '100', NULL, 'BILLPLZ', NULL, 'r7rt9dfd', NULL, NULL, 'PENDING', 'TBC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-03-08 07:43:52', '2021-03-08 07:44:44');

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_list`
--

INSERT INTO `order_list` (`id`, `user_uid`, `product_uid`, `product_name`, `main_product_uid`, `order_id`, `quantity`, `original_price`, `final_price`, `discount`, `totalPrice`, `status`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d', '214dda0557ae73d55e736a6248bc245e', 'iPhone 11 Pro Max', '7c641a903a79a08b6a96376e8548c6dc', '9349aede685bae49c49b0b895e465f6d1609820390', '2', '4288', NULL, NULL, '8576', 'Sold', '2021-01-05 04:19:50', '2021-01-05 04:20:10'),
(2, '9349aede685bae49c49b0b895e465f6d', '4d02d9cb944947633c0d9b5cbee72817', 'Samsung Note 10 Plus', '7c641a903a79a08b6a96376e8548c6dc', '9349aede685bae49c49b0b895e465f6d1609820390', '2', '3388', NULL, NULL, '6776', 'Sold', '2021-01-05 04:19:50', '2021-01-05 04:20:10'),
(3, '9349aede685bae49c49b0b895e465f6d', '1cb5da24f138488567b12bc554b8b488', 'FC Bayern', 'b819df1515b74e6c0f1106ae4b86146d', '9349aede685bae49c49b0b895e465f6d1609820390', '2', '388', NULL, NULL, '776', 'Sold', '2021-01-05 04:19:50', '2021-01-05 04:20:10'),
(4, '9349aede685bae49c49b0b895e465f6d', '1cb5da24f138488567b12bc554b8b488', 'FC Bayern', 'b819df1515b74e6c0f1106ae4b86146d', '9349aede685bae49c49b0b895e465f6d1609820529', '1', '388', NULL, NULL, '388', 'Sold', '2021-01-05 04:22:09', '2021-01-05 04:22:33'),
(5, '9349aede685bae49c49b0b895e465f6d', '6162a37b397d5fb42e6550439c6b9ed9', 'Real Madrid', 'b819df1515b74e6c0f1106ae4b86146d', '9349aede685bae49c49b0b895e465f6d1609820529', '3', '338', NULL, NULL, '1014', 'Sold', '2021-01-05 04:22:09', '2021-01-05 04:22:33'),
(6, '9349aede685bae49c49b0b895e465f6d', 'bc08fb0c69d2c88c7bb22f4941d3405b', 'Manchester City', 'b819df1515b74e6c0f1106ae4b86146d', '9349aede685bae49c49b0b895e465f6d1609820529', '2', '358', NULL, NULL, '716', 'Sold', '2021-01-05 04:22:09', '2021-01-05 04:22:33'),
(7, '5d6368155bcc2ec3b4234b250c737ac4', '214dda0557ae73d55e736a6248bc245e', 'iPhone 11 Pro Max', '7c641a903a79a08b6a96376e8548c6dc', '5d6368155bcc2ec3b4234b250c737ac41609820623', '1', '4288', NULL, NULL, '4288', 'Sold', '2021-01-05 04:23:43', '2021-01-05 04:23:52'),
(8, '5d6368155bcc2ec3b4234b250c737ac4', '4d02d9cb944947633c0d9b5cbee72817', 'Samsung Note 10 Plus', '7c641a903a79a08b6a96376e8548c6dc', '5d6368155bcc2ec3b4234b250c737ac41609820623', '2', '3388', NULL, NULL, '6776', 'Sold', '2021-01-05 04:23:43', '2021-01-05 04:23:52'),
(9, 'fdefd9112e3579886dfbb9e04d7f928d', '81df75f923546629de9804966d594b08', 'Juventus', 'b819df1515b74e6c0f1106ae4b86146d', 'fdefd9112e3579886dfbb9e04d7f928d1609820691', '2', '328', NULL, NULL, '656', 'Sold', '2021-01-05 04:24:51', '2021-01-05 04:25:01'),
(10, 'fdefd9112e3579886dfbb9e04d7f928d', '6162a37b397d5fb42e6550439c6b9ed9', 'Real Madrid', 'b819df1515b74e6c0f1106ae4b86146d', 'fdefd9112e3579886dfbb9e04d7f928d1609820691', '3', '338', NULL, NULL, '1014', 'Sold', '2021-01-05 04:24:51', '2021-01-05 04:25:01'),
(11, 'fdefd9112e3579886dfbb9e04d7f928d', '214dda0557ae73d55e736a6248bc245e', 'iPhone 11 Pro Max', '7c641a903a79a08b6a96376e8548c6dc', 'fdefd9112e3579886dfbb9e04d7f928d1609820733', '1', '4288', NULL, NULL, '4288', 'Sold', '2021-01-05 04:25:33', '2021-01-05 04:25:52'),
(12, '9349aede685bae49c49b0b895e465f6d', '5a80be29c265358a68547dd47d544b65', 'INABA - Chic + Veg + Cheese', 'c634208e1ac402adcf6e372882ce99b3', '9349aede685bae49c49b0b895e465f6d1609918687', '1', '6', NULL, NULL, '6', 'Sold', '2021-01-06 07:38:07', '2021-01-06 07:38:12'),
(13, '9349aede685bae49c49b0b895e465f6d', 'dc85597f49bf33a957d3cb8be0b38c79', 'Parie Goat Milk Powder for Puppies', '90efab9d88a5db154e7f908371e1f7cd', '9349aede685bae49c49b0b895e465f6d1609918687', '1', '70', NULL, NULL, '70', 'Sold', '2021-01-06 07:38:07', '2021-01-06 07:38:12'),
(14, '9349aede685bae49c49b0b895e465f6d', 'dc85597f49bf33a957d3cb8be0b38c79', 'Parie Goat Milk Powder for Puppies', '90efab9d88a5db154e7f908371e1f7cd', '9349aede685bae49c49b0b895e465f6d1609918776', '1', '70', NULL, NULL, '70', 'Sold', '2021-01-06 07:39:36', '2021-01-06 07:39:45'),
(15, '9349aede685bae49c49b0b895e465f6d', '3196e420bc90b9611bc93d876168fbe3', 'Parie Goat Milk Powder for Kittens', '7ba69bf0cc5f01ae0562977fca09ea92', '9349aede685bae49c49b0b895e465f6d1609919207', '1', '70', NULL, NULL, '70', 'Sold', '2021-01-06 07:46:47', '2021-01-06 07:46:52'),
(16, '5633b78c7f79f1dfc93870d84c00abb3', 'dc85597f49bf33a957d3cb8be0b38c79', 'Parie Goat Milk Powder for Puppies', '90efab9d88a5db154e7f908371e1f7cd', '5633b78c7f79f1dfc93870d84c00abb31610086004', '1', '70', NULL, NULL, '70', 'Sold', '2021-01-08 06:06:44', '2021-01-08 06:07:02'),
(17, '97af0bfb9c1f8ca19db23ac31139d6bc', '5cff63e3ad7dbbc875748cac752eb15f', 'Harrington&#039;s Dog Food Complete Turkey And Vegetables', 'de6b3e53200445d688f5797620062c9b', '97af0bfb9c1f8ca19db23ac31139d6bc1611070525', '2', '100', NULL, NULL, '200', 'Sold', '2021-01-19 15:35:26', '2021-01-19 15:35:49'),
(18, 'd156938c9b63fd253e9cddf89eba10e7', '54c8ac1ed6d364f6a25d90317657800d', 'Royal Canin British Short Hair Kitten 2Kg', '0f7b32cdabccd5a533bea31c743144ab', 'd156938c9b63fd253e9cddf89eba10e71611134052', '1', '80', NULL, NULL, '80', 'Sold', '2021-01-20 09:14:12', '2021-01-20 09:14:44'),
(19, 'dc0c19ea4deeb8d1e28e9064b6f8d7cf', '5cff63e3ad7dbbc875748cac752eb15f', 'Harrington&#039;s Dog Food Complete Turkey And Vegetables', 'de6b3e53200445d688f5797620062c9b', 'dc0c19ea4deeb8d1e28e9064b6f8d7cf1611307281', '3', '100', NULL, NULL, '300', 'Sold', '2021-01-22 09:21:22', '2021-01-22 09:21:29'),
(20, '9349aede685bae49c49b0b895e465f6d', '5a80be29c265358a68547dd47d544b65', 'INABA - Chic + Veg + Cheese', 'c634208e1ac402adcf6e372882ce99b3', '9349aede685bae49c49b0b895e465f6d1612173519', '1', '6', NULL, NULL, '6', 'Sold', '2021-02-01 09:58:39', '2021-02-01 09:58:44'),
(21, '9349aede685bae49c49b0b895e465f6d', 'dc85597f49bf33a957d3cb8be0b38c79', 'Parie Goat Milk Powder for Puppies', '90efab9d88a5db154e7f908371e1f7cd', '9349aede685bae49c49b0b895e465f6d1612173624', '1', '70', NULL, NULL, '70', 'Sold', '2021-02-01 10:00:24', '2021-02-01 10:00:28'),
(22, '9349aede685bae49c49b0b895e465f6d', '54c8ac1ed6d364f6a25d90317657800d', 'Royal Canin British Short Hair Kitten 2Kg', '0f7b32cdabccd5a533bea31c743144ab', '9349aede685bae49c49b0b895e465f6d1614414219', '1', '80', NULL, NULL, '80', 'Pending', '2021-02-27 08:23:39', '2021-02-27 08:23:39'),
(23, '9349aede685bae49c49b0b895e465f6d', '5cff63e3ad7dbbc875748cac752eb15f', 'Harrington&#039;s Dog Food Complete Turkey And Vegetables', 'de6b3e53200445d688f5797620062c9b', '9349aede685bae49c49b0b895e465f6d1615189380', '1', '100', NULL, NULL, '100', 'Sold', '2021-03-08 07:43:00', '2021-03-08 07:43:52');

-- --------------------------------------------------------

--
-- Table structure for table `pet_details`
--

CREATE TABLE `pet_details` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pet_details`
--

INSERT INTO `pet_details` (`id`, `uid`, `name`, `sku`, `slug`, `keyword_one`, `keyword_two`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `type`, `details`, `details_two`, `details_three`, `details_four`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `date_created`, `date_updated`) VALUES
(1, '17f37cc889a754f723f2eb48adff467f', ' ', 'MAY-PF-04', ' ', ' ', NULL, '3688', '6 weeks', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Large', 'Available', 'No', 'West Highland Terrier', 'c61de2ff2583ec2c9f8ce06db86a7a97', 'puppy', 'Alaskan Malamute x Wooly Husky  DOB on 20th March 2020', NULL, NULL, NULL, '416349175', 'Penang', '160584626617f37cc889a754f723f2eb48adff467fpuppy1a.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1b.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1c.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1d.jpg', NULL, NULL, '160584626617f37cc889a754f723f2eb48adff467fpuppy1a.jpg', '2020-11-20 04:24:19', '2020-11-20 04:24:19');

-- --------------------------------------------------------

--
-- Table structure for table `preorder_list`
--

CREATE TABLE `preorder_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `preorder_list`
--

INSERT INTO `preorder_list` (`id`, `user_uid`, `product_uid`, `product_name`, `main_product_uid`, `order_id`, `quantity`, `original_price`, `final_price`, `discount`, `totalPrice`, `status`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d', '214dda0557ae73d55e736a6248bc245e', 'iPhone 11 Pro Max', '7c641a903a79a08b6a96376e8548c6dc', NULL, '2', '4288', NULL, NULL, '8576', 'Sold', '2021-01-05 04:19:11', '2021-01-05 04:19:50'),
(2, '9349aede685bae49c49b0b895e465f6d', '5f1e0ee1eae175e525b1a4fd6b438e2a', 'iPhone 11 Pro', '7c641a903a79a08b6a96376e8548c6dc', NULL, '1', '3899', NULL, NULL, '3899', 'Delete', '2021-01-05 04:19:22', '2021-01-05 04:19:30'),
(3, '9349aede685bae49c49b0b895e465f6d', '4d02d9cb944947633c0d9b5cbee72817', 'Samsung Note 10 Plus', '7c641a903a79a08b6a96376e8548c6dc', NULL, '2', '3388', NULL, NULL, '6776', 'Sold', '2021-01-05 04:19:28', '2021-01-05 04:19:50'),
(4, '9349aede685bae49c49b0b895e465f6d', '1cb5da24f138488567b12bc554b8b488', 'FC Bayern', 'b819df1515b74e6c0f1106ae4b86146d', NULL, '2', '388', NULL, NULL, '776', 'Sold', '2021-01-05 04:19:35', '2021-01-05 04:19:50'),
(5, '9349aede685bae49c49b0b895e465f6d', '1cb5da24f138488567b12bc554b8b488', 'FC Bayern', 'b819df1515b74e6c0f1106ae4b86146d', NULL, '1', '388', NULL, NULL, '388', 'Sold', '2021-01-05 04:20:59', '2021-01-05 04:22:09'),
(6, '9349aede685bae49c49b0b895e465f6d', '81df75f923546629de9804966d594b08', 'Juventus', 'b819df1515b74e6c0f1106ae4b86146d', NULL, '2', '328', NULL, NULL, '656', 'Delete', '2021-01-05 04:21:05', '2021-01-05 04:21:13'),
(7, '9349aede685bae49c49b0b895e465f6d', '6162a37b397d5fb42e6550439c6b9ed9', 'Real Madrid', 'b819df1515b74e6c0f1106ae4b86146d', NULL, '3', '338', NULL, NULL, '1014', 'Sold', '2021-01-05 04:21:11', '2021-01-05 04:22:09'),
(8, '9349aede685bae49c49b0b895e465f6d', 'bc08fb0c69d2c88c7bb22f4941d3405b', 'Manchester City', 'b819df1515b74e6c0f1106ae4b86146d', NULL, '2', '358', NULL, NULL, '716', 'Sold', '2021-01-05 04:21:19', '2021-01-05 04:22:09'),
(9, '9349aede685bae49c49b0b895e465f6d', '1cb5da24f138488567b12bc554b8b488', 'FC Bayern', 'b819df1515b74e6c0f1106ae4b86146d', NULL, '1', '388', NULL, NULL, '388', 'Delete', '2021-01-05 04:21:41', '2021-01-05 04:21:48'),
(10, '5d6368155bcc2ec3b4234b250c737ac4', '214dda0557ae73d55e736a6248bc245e', 'iPhone 11 Pro Max', '7c641a903a79a08b6a96376e8548c6dc', NULL, '1', '4288', NULL, NULL, '4288', 'Sold', '2021-01-05 04:23:27', '2021-01-05 04:23:43'),
(11, '5d6368155bcc2ec3b4234b250c737ac4', '5f1e0ee1eae175e525b1a4fd6b438e2a', 'iPhone 11 Pro', '7c641a903a79a08b6a96376e8548c6dc', NULL, '1', '3899', NULL, NULL, '3899', 'Delete', '2021-01-05 04:23:34', '2021-01-05 04:23:42'),
(12, '5d6368155bcc2ec3b4234b250c737ac4', '4d02d9cb944947633c0d9b5cbee72817', 'Samsung Note 10 Plus', '7c641a903a79a08b6a96376e8548c6dc', NULL, '2', '3388', NULL, NULL, '6776', 'Sold', '2021-01-05 04:23:41', '2021-01-05 04:23:43'),
(13, 'fdefd9112e3579886dfbb9e04d7f928d', '1cb5da24f138488567b12bc554b8b488', 'FC Bayern', 'b819df1515b74e6c0f1106ae4b86146d', NULL, '1', '388', NULL, NULL, '388', 'Delete', '2021-01-05 04:24:34', '2021-01-05 04:24:50'),
(14, 'fdefd9112e3579886dfbb9e04d7f928d', '81df75f923546629de9804966d594b08', 'Juventus', 'b819df1515b74e6c0f1106ae4b86146d', NULL, '2', '328', NULL, NULL, '656', 'Sold', '2021-01-05 04:24:40', '2021-01-05 04:24:51'),
(15, 'fdefd9112e3579886dfbb9e04d7f928d', '6162a37b397d5fb42e6550439c6b9ed9', 'Real Madrid', 'b819df1515b74e6c0f1106ae4b86146d', NULL, '3', '338', NULL, NULL, '1014', 'Sold', '2021-01-05 04:24:46', '2021-01-05 04:24:51'),
(16, 'fdefd9112e3579886dfbb9e04d7f928d', '214dda0557ae73d55e736a6248bc245e', 'iPhone 11 Pro Max', '7c641a903a79a08b6a96376e8548c6dc', NULL, '1', '4288', NULL, NULL, '4288', 'Sold', '2021-01-05 04:25:31', '2021-01-05 04:25:33'),
(17, '9349aede685bae49c49b0b895e465f6d', '5a80be29c265358a68547dd47d544b65', 'INABA - Chic + Veg + Cheese', 'c634208e1ac402adcf6e372882ce99b3', NULL, '1', '6', NULL, NULL, '6', 'Sold', '2021-01-06 06:19:08', '2021-01-06 07:38:07'),
(18, '9349aede685bae49c49b0b895e465f6d', 'dc85597f49bf33a957d3cb8be0b38c79', 'Parie Goat Milk Powder for Puppies', '90efab9d88a5db154e7f908371e1f7cd', NULL, '1', '70', NULL, NULL, '70', 'Delete', '2021-01-06 07:17:12', '2021-01-06 07:38:04'),
(19, '9349aede685bae49c49b0b895e465f6d', 'dc85597f49bf33a957d3cb8be0b38c79', 'Parie Goat Milk Powder for Puppies', '90efab9d88a5db154e7f908371e1f7cd', NULL, '1', '70', NULL, NULL, '70', 'Sold', '2021-01-06 07:37:58', '2021-01-06 07:38:07'),
(20, '9349aede685bae49c49b0b895e465f6d', 'dc85597f49bf33a957d3cb8be0b38c79', 'Parie Goat Milk Powder for Puppies', '90efab9d88a5db154e7f908371e1f7cd', NULL, '1', '70', NULL, NULL, '70', 'Sold', '2021-01-06 07:39:34', '2021-01-06 07:39:36'),
(21, '9349aede685bae49c49b0b895e465f6d', '3196e420bc90b9611bc93d876168fbe3', 'Parie Goat Milk Powder for Kittens', '7ba69bf0cc5f01ae0562977fca09ea92', NULL, '1', '70', NULL, NULL, '70', 'Sold', '2021-01-06 07:46:45', '2021-01-06 07:46:47'),
(22, '5633b78c7f79f1dfc93870d84c00abb3', 'dc85597f49bf33a957d3cb8be0b38c79', 'Parie Goat Milk Powder for Puppies', '90efab9d88a5db154e7f908371e1f7cd', NULL, '1', '70', NULL, NULL, '70', 'Sold', '2021-01-08 06:06:42', '2021-01-08 06:06:44'),
(23, '97af0bfb9c1f8ca19db23ac31139d6bc', '5cff63e3ad7dbbc875748cac752eb15f', 'Harrington&#039;s Dog Food Complete Turkey And Vegetables', 'de6b3e53200445d688f5797620062c9b', NULL, '2', '100', NULL, NULL, '200', 'Sold', '2021-01-19 15:35:22', '2021-01-19 15:35:25'),
(24, 'd156938c9b63fd253e9cddf89eba10e7', '54c8ac1ed6d364f6a25d90317657800d', 'Royal Canin British Short Hair Kitten 2Kg', '0f7b32cdabccd5a533bea31c743144ab', NULL, '1', '80', NULL, NULL, '80', 'Sold', '2021-01-20 09:13:34', '2021-01-20 09:14:12'),
(25, 'dc0c19ea4deeb8d1e28e9064b6f8d7cf', '5cff63e3ad7dbbc875748cac752eb15f', 'Harrington&#039;s Dog Food Complete Turkey And Vegetables', 'de6b3e53200445d688f5797620062c9b', NULL, '3', '100', NULL, NULL, '300', 'Sold', '2021-01-22 09:20:33', '2021-01-22 09:21:21'),
(26, '9349aede685bae49c49b0b895e465f6d', '5a80be29c265358a68547dd47d544b65', 'INABA - Chic + Veg + Cheese', 'c634208e1ac402adcf6e372882ce99b3', NULL, '1', '6', NULL, NULL, '6', 'Delete', '2021-02-01 08:00:56', '2021-02-01 09:59:59'),
(27, '9349aede685bae49c49b0b895e465f6d', '5a80be29c265358a68547dd47d544b65', 'INABA - Chic + Veg + Cheese', 'c634208e1ac402adcf6e372882ce99b3', NULL, '1', '6', NULL, NULL, '6', 'Delete', '2021-02-01 08:01:18', '2021-02-01 09:59:59'),
(28, '9349aede685bae49c49b0b895e465f6d', '5a80be29c265358a68547dd47d544b65', 'INABA - Chic + Veg + Cheese', 'c634208e1ac402adcf6e372882ce99b3', NULL, '1', '6', NULL, NULL, '6', 'Delete', '2021-02-01 08:01:24', '2021-02-01 10:00:00'),
(29, '9349aede685bae49c49b0b895e465f6d', '5a80be29c265358a68547dd47d544b65', 'INABA - Chic + Veg + Cheese', 'c634208e1ac402adcf6e372882ce99b3', NULL, '1', '6', NULL, NULL, '6', 'Delete', '2021-02-01 09:51:36', '2021-02-01 10:00:00'),
(30, '9349aede685bae49c49b0b895e465f6d', 'dc85597f49bf33a957d3cb8be0b38c79', 'Parie Goat Milk Powder for Puppies', '90efab9d88a5db154e7f908371e1f7cd', NULL, '1', '70', NULL, NULL, '70', 'Sold', '2021-02-01 10:00:20', '2021-02-01 10:00:24'),
(31, '9349aede685bae49c49b0b895e465f6d', '54c8ac1ed6d364f6a25d90317657800d', 'Royal Canin British Short Hair Kitten 2Kg', '0f7b32cdabccd5a533bea31c743144ab', NULL, '1', '80', NULL, NULL, '80', 'Delete', '2021-02-27 08:21:59', '2021-02-27 08:39:27'),
(32, '9349aede685bae49c49b0b895e465f6d', '54c8ac1ed6d364f6a25d90317657800d', 'Royal Canin British Short Hair Kitten 2Kg', '0f7b32cdabccd5a533bea31c743144ab', NULL, '1', '80', NULL, NULL, '80', 'Delete', '2021-02-27 08:22:37', '2021-02-27 08:39:26'),
(33, '9349aede685bae49c49b0b895e465f6d', '54c8ac1ed6d364f6a25d90317657800d', 'Royal Canin British Short Hair Kitten 2Kg', '0f7b32cdabccd5a533bea31c743144ab', NULL, '1', '80', NULL, NULL, '80', 'Delete', '2021-02-27 08:23:34', '2021-02-27 08:39:23'),
(34, '9349aede685bae49c49b0b895e465f6d', '54c8ac1ed6d364f6a25d90317657800d', 'Royal Canin British Short Hair Kitten 2Kg', '0f7b32cdabccd5a533bea31c743144ab', NULL, '1', '80', NULL, NULL, '80', 'Delete', '2021-02-27 08:39:16', '2021-02-27 08:39:25'),
(35, '9349aede685bae49c49b0b895e465f6d', '5cff63e3ad7dbbc875748cac752eb15f', 'Harrington&#039;s Dog Food Complete Turkey And Vegetables', 'de6b3e53200445d688f5797620062c9b', NULL, '1', '100', NULL, NULL, '100', 'Sold', '2021-03-08 07:39:15', '2021-03-08 07:43:00');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `diamond` varchar(255) DEFAULT NULL,
  `minPrice` decimal(15,2) DEFAULT NULL,
  `midPrice` decimal(15,2) DEFAULT NULL,
  `maxPrice` decimal(15,2) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `description_two` text DEFAULT NULL,
  `description_three` text DEFAULT NULL,
  `description_four` text DEFAULT NULL,
  `description_five` text DEFAULT NULL,
  `description_six` text DEFAULT NULL,
  `rating` varchar(255) DEFAULT '0',
  `animal_type` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `category`, `brand`, `name`, `sku`, `slug`, `price`, `diamond`, `minPrice`, `midPrice`, `maxPrice`, `feature`, `status`, `quantity`, `description`, `description_two`, `description_three`, `description_four`, `description_five`, `description_six`, `rating`, `animal_type`, `keyword_one`, `link`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `date_created`, `date_updated`) VALUES
(1, '90efab9d88a5db154e7f908371e1f7cd', 'Dog Wet Food', 'Hills', 'Parie Goat Milk Powder for Puppies 500gm', 'Parie Goat Milk Powder for Puppies 500gm', 'Parie Goat Milk Powder for Puppies 500gm', NULL, NULL, '70.00', '70.00', '70.00', 'Yes', 'Available', NULL, 'Parie goat milk powder products are specially formulated for puppies, pregnant and lactating dogs.', 'Our pure goat milk powder are imported from Netherland.', 'Parie goat milk powder is a premium milk supplements that contains DHA &amp; ARA that is needed by dogs.', 'Contains DHA &amp; ARAFatty acids make up 60% of the brain.', 'Two of these fatty acids play a critical role in supporting puppies’ brain and eye development: DHA (docosahexaenoic acid) &amp; ARA (arachidonic acid).', 'DHA &amp; ARA are associated with the development of puppies’ brain and eventual her ability to learn, judge, and concentrate.', '5', NULL, 'Parie Goat Milk Powder for Puppies,', '', '1609909957milk-for-puppy2.png', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-06 05:12:08', '2021-02-01 07:09:18'),
(2, '7ba69bf0cc5f01ae0562977fca09ea92', 'Cat Wet Food', 'Hills', 'Parie Goat Milk Powder for Kittenss', 'Parie Goat Milk Powder for Kittenss', 'Parie Goat Milk Powder for Kittenss', NULL, NULL, '70.00', '70.00', '70.00', 'Yes', 'Available', NULL, 'PARIE&#039;s goat milk carries similarities as breastfeeding as it is tolerated for being less allergenic to cats.', 'The properties of goat milk which comprises smaller fat molecules, looser curds formation and higher concentration of small chain fatty acids will enhance cat&#039;s digestion.', 'We all know that cats like milk.', 'Research shows that milk calms cats and gives them pleasure.', 'Besides, cats need to consume sufficient nutrition to grow healthily.', '', '0', NULL, 'Parie Goat Milk Powder for Kittens,', '', '1609910471milk-for-kitten.png', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-06 05:20:12', '2021-02-01 07:09:11'),
(3, 'c634208e1ac402adcf6e372882ce99b3', 'Dog Wet Food', 'Monge', 'INABA Twin Packs Recipe Dog Pet Wet Food 80g', 'INABA Twin Packs Recipe Dog Pet Wet Food 80g', 'INABA Twin Packs Recipe Dog Pet Wet Food 80g', NULL, NULL, '6.00', '6.00', '6.00', 'Yes', 'Available', NULL, 'Made with their favorite high-quality ingredients.', 'Comes in convenient individual pouches for maximum freshness.', 'Your dog will always enjoy enticing aroma and irresistible taste!', '', '', '', '5', NULL, 'INABA Twin Packs Recipe,', '', '1609913589twin1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-06 06:02:11', '2021-02-01 07:09:39'),
(4, '0f7b32cdabccd5a533bea31c743144ab', 'Cat Dry Food', 'Royal Canin', 'Royal Canin FBN British Short Hair Kitten 2Kg', 'Royal Canin FBN British Short Hair Kitten 2Kg', 'Royal Canin FBN British Short Hair Kitten 2Kg', NULL, NULL, '80.00', '80.00', '80.00', 'Yes', 'Available', NULL, 'This specially designed moon-shaped kibble is adapted in size and texture to be easy for the British Shorthair kitten to grasp.', 'The unique kibble encourages chewing to help support oral hygiene.', 'Exclusive formula - Psyllium &amp; L-carnitine', 'During the growth period, the kitten’s digestive system is immature and continues developing gradually.', 'Enriched with L-carnitine, involved in healthy fat metabolism.', '', '0', NULL, 'Royal Canin FBN British Short Hair Kitten,', '112184033', '1609914420royal.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-06 06:25:24', '2021-02-01 07:04:32'),
(5, 'de6b3e53200445d688f5797620062c9b', 'Dog Dry Food', 'Iskhan', 'Harrington&#039;s Dog Food Complete Turkey And Vegetables Dry Mix 15kg', 'Harrington&#039;s Dog Food Complete Turkey And Vegetables Dry Mix 15kg', 'Harrington&#039;s Dog Food Complete Turkey And Vegetables Dry Mix 15kg', NULL, NULL, '100.00', '100.00', '100.00', 'Yes', 'Available', NULL, 'With added omega oils, natural antioxidants, essential amino acids and nutrient rich kelp.', 'It can help to improve digestion, reduce flatulent odours and help improve skin and coat condition.', 'Packed in compostable 2kg paper bags and recyclable 15kg plastic sacks it is a completely natural dry dog food.', '', '', '', '0', NULL, 'Harrington&#039;s Dog Food, Turkey and vegetable,', '116644786', '1609915208de6b3e53200445d688f5797620062c9bha3.jpg', '1609915223de6b3e53200445d688f5797620062c9bha2.jpg', '1609915399de6b3e53200445d688f5797620062c9bhar4.jpg', NULL, NULL, NULL, NULL, '2021-01-06 06:39:36', '2021-02-01 07:09:49'),
(6, 'c42d1def318a860ee24cbd2d25805c23', 'Dog Dry Food', NULL, 'admin', NULL, 'admin', NULL, NULL, '70.00', '70.00', '70.00', NULL, 'Delete', NULL, 'Parie goat milk powder products are specially formulated for puppies, pregnant and lactating dogs.', '', '', '', '', '', '0', NULL, '', '', '16099252204paws.png', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-06 09:26:47', '2021-01-06 09:27:16'),
(7, '2fcab53d2235eff90661cbd69c95ac1e', 'Dog Wet Food', 'Royal Canin', 'asdsadas', NULL, 'asdsadas', NULL, NULL, '66.00', '477.00', '888.00', NULL, 'Available', NULL, 'asdasdasasdasdasdasasdasdasd', '', '', '', '', '', '0', NULL, 'asdsadas', '', '16178132442fcab53d2235eff90661cbd69c95ac1efrog.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-07 16:33:51', '2021-04-07 16:34:42');

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` int(10) NOT NULL,
  `final_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `original_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `discount_given` decimal(50,0) NOT NULL COMMENT 'in points',
  `totalProductPrice` decimal(50,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `puppy`
--

CREATE TABLE `puppy` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) NOT NULL,
  `img_name` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puppy`
--

INSERT INTO `puppy` (`id`, `uid`, `name`, `sku`, `slug`, `keyword_one`, `keyword_two`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `featured_seller`, `details`, `details_two`, `details_three`, `details_four`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `img_name`, `date_created`, `date_updated`) VALUES
(1, '17f37cc889a754f723f2eb48adff467f', ' ', 'MAY-PF-04', ' ', ' ', NULL, '3688', '6 weeks', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Large', 'Available', 'No', 'West Highland Terrier', 'c61de2ff2583ec2c9f8ce06db86a7a97', 'Yes', 'Alaskan Malamute x Wooly Husky  DOB on 20th March 2020', NULL, NULL, NULL, '416349175', 'Penang', '160584626617f37cc889a754f723f2eb48adff467fpuppy1a.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1b.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1c.jpg', '160584626617f37cc889a754f723f2eb48adff467fpuppy1d.jpg', NULL, NULL, '160584626617f37cc889a754f723f2eb48adff467fpuppy1a.jpg', NULL, '2020-11-20 04:24:19', '2020-11-20 04:24:19');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` text DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `name`, `image`, `status`, `date_created`, `date_updated`) VALUES
(1, '0', 'No Rating Yet', 'Available', '2021-01-04 10:52:36', '2021-01-04 10:52:36'),
(2, '1', '<img src=\"img/yellow-star.png\" class=\"star-img\">', 'Available', '2021-01-04 10:52:44', '2021-01-04 10:52:44'),
(3, '2', '<img src=\"img/yellow-star.png\" class=\"star-img\"><img src=\"img/yellow-star.png\" class=\"star-img\">', 'Available', '2021-01-04 10:52:53', '2021-01-04 10:52:53'),
(4, '3', '<img src=\"img/yellow-star.png\" class=\"star-img\"><img src=\"img/yellow-star.png\" class=\"star-img\"><img src=\"img/yellow-star.png\" class=\"star-img\">', 'Available', '2021-01-04 10:53:04', '2021-01-04 10:53:04'),
(5, '4', '<img src=\"img/yellow-star.png\" class=\"star-img\"><img src=\"img/yellow-star.png\" class=\"star-img\"><img src=\"img/yellow-star.png\" class=\"star-img\"><img src=\"img/yellow-star.png\" class=\"star-img\">', 'Available', '2021-01-04 10:53:41', '2021-01-04 10:53:41'),
(6, '5', '<img src=\"img/yellow-star.png\" class=\"star-img\"><img src=\"img/yellow-star.png\" class=\"star-img\"><img src=\"img/yellow-star.png\" class=\"star-img\"><img src=\"img/yellow-star.png\" class=\"star-img\"><img src=\"img/yellow-star.png\" class=\"star-img\">', 'Available', '2021-01-05 06:15:10', '2021-01-05 06:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `reported_article`
--

CREATE TABLE `reported_article` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `article_uid` varchar(255) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reported_reviews`
--

CREATE TABLE `reported_reviews` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `article_uid` varchar(255) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reptile`
--

CREATE TABLE `reptile` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) NOT NULL,
  `age` varchar(255) NOT NULL,
  `vaccinated` varchar(255) NOT NULL,
  `dewormed` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `feature` varchar(255) NOT NULL,
  `breed` varchar(255) NOT NULL,
  `seller` varchar(255) NOT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `company_uid` varchar(255) DEFAULT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `rating` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `likes` varchar(255) DEFAULT '0',
  `report_ppl` varchar(255) DEFAULT NULL,
  `report_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `uid`, `company_uid`, `author_uid`, `author_name`, `title`, `paragraph_one`, `paragraph_two`, `paragraph_three`, `rating`, `image`, `likes`, `report_ppl`, `report_id`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, '85d1b82093320ec88262e2472985c712', '90efab9d88a5db154e7f908371e1f7cd', '9349aede685bae49c49b0b895e465f6d', 'user', '', 'My pet loves it.', NULL, NULL, '5', '85d1b82093320ec88262e2472985c712milk-for-puppy2.png', '1', NULL, NULL, NULL, 'Yes', '2021-01-06 07:44:26', '2021-01-06 07:46:20'),
(2, '69c6cb2ef644737e12698ee7d64d9fdf', '7ba69bf0cc5f01ae0562977fca09ea92', '9349aede685bae49c49b0b895e465f6d', 'user', '', 'Good product.', NULL, NULL, '5', '69c6cb2ef644737e12698ee7d64d9fdf', '0', NULL, NULL, NULL, 'Rejected', '2021-01-06 07:47:45', '2021-01-06 07:51:35'),
(3, '0849e8bafa1b9b17c09156d1506dcf81', 'c634208e1ac402adcf6e372882ce99b3', '9349aede685bae49c49b0b895e465f6d', 'user', '', 'Good', NULL, NULL, '5', '0849e8bafa1b9b17c09156d1506dcf81', '0', NULL, NULL, NULL, 'Yes', '2021-01-29 11:54:29', '2021-01-29 11:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `reviews_respond`
--

CREATE TABLE `reviews_respond` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `review_uid` varchar(255) DEFAULT NULL,
  `like_amount` int(255) DEFAULT NULL,
  `dislike_amount` int(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews_respond`
--

INSERT INTO `reviews_respond` (`id`, `uid`, `username`, `review_uid`, `like_amount`, `dislike_amount`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d', 'user', '85d1b82093320ec88262e2472985c712', 1, NULL, '2021-01-06 07:46:17', '2021-01-06 07:46:17'),
(2, '9349aede685bae49c49b0b895e465f6d', 'user', '85d1b82093320ec88262e2472985c712', NULL, 1, '2021-01-06 07:46:19', '2021-01-06 07:46:19'),
(3, '9349aede685bae49c49b0b895e465f6d', 'user', '85d1b82093320ec88262e2472985c712', 1, NULL, '2021-01-06 07:46:20', '2021-01-06 07:46:20');

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE `seller` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `company_name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_person_no` varchar(255) DEFAULT NULL,
  `sec_contact_person` varchar(255) DEFAULT NULL,
  `sec_contact_person_no` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `cert` varchar(255) DEFAULT NULL,
  `services` varchar(255) DEFAULT NULL,
  `breed_type` varchar(255) DEFAULT NULL,
  `other_info` text DEFAULT NULL,
  `info_two` text DEFAULT NULL,
  `info_three` text DEFAULT NULL,
  `info_four` text DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `company_pic` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `credit` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seller`
--

INSERT INTO `seller` (`id`, `uid`, `company_name`, `slug`, `registration_no`, `contact_no`, `email`, `address`, `state`, `country`, `account_status`, `featured_seller`, `contact_person`, `contact_person_no`, `sec_contact_person`, `sec_contact_person_no`, `experience`, `cert`, `services`, `breed_type`, `other_info`, `info_two`, `info_three`, `info_four`, `company_logo`, `company_pic`, `rating`, `credit`, `date_created`, `date_updated`) VALUES
(1, 'c61de2ff2583ec2c9f8ce06db86a7a97', 'Wiskey', 'Wiskey', 'wiskeypg', 'https://api.whatsapp.com/send?phone=60121234567', 'wiskey100@gmail.com', '123, Wiskey Building, Jln Wiskey, 11950 PG.', 'Penang', NULL, 'Active', NULL, 'Wiskey Admin', '0123456789', NULL, NULL, '10', 'QWERTY, ASD', '1,2,3', 'Breed 1, Breed 2, Breed 3', 'Other Info 1', 'Other Info 2', 'Other Info 3', 'Other Info 4', 'c61de2ff2583ec2c9f8ce06db86a7a971605842716.png', 'c61de2ff2583ec2c9f8ce06db86a7a971605842878.png', '4', '94', '2020-11-20 03:20:39', '2020-11-20 03:20:39'),
(2, '96efa305ea0de6aad93f3fe561f14bcc', 'Win Win Pet House', 'Win-Win-Pet-House', 'winwinpethousejb', 'https://api.whatsapp.com/send?phone=60107418520', 'win125@gmail.com', '234, Win Win Building, Jln Win, 00100 JB.', 'Johor', NULL, 'Active', NULL, 'WWPH Admin', '0127418520', NULL, NULL, '10', 'QWERTY, ZXC', '1, 4', 'Breed 1, Breed 2', 'Other Info 1', 'Other Info 12', 'Other Info 123', '', '96efa305ea0de6aad93f3fe561f14bcc1605843836.png', '96efa305ea0de6aad93f3fe561f14bcc1605843843.png', NULL, '122', '2020-11-20 03:43:46', '2020-11-20 03:43:46'),
(3, 'a55f9ab3c32ebbc3e0baaa00ba07303b', 'Durain Sam Gee Kann', 'Durain-Sam-Gee-Kann', 'duriansamgeekannkl', 'https://api.whatsapp.com/send?phone=60107419963', 'duriansgk150@gmail.com', '345, Durian SGK Building, Jln Win, 88120 KL.', 'Kuala Lumpur', NULL, 'Active', NULL, 'Durain Sam Gee Kann Admin', '0127419963', NULL, NULL, '5', 'QWERT, QWERTY', '1, 4, 5', 'Breed 1, Breed 3', 'Other Info 1', 'Other Info 12', 'Other Info 123', 'Other Info 1234', 'a55f9ab3c32ebbc3e0baaa00ba07303b1605844161.png', 'a55f9ab3c32ebbc3e0baaa00ba07303b1605844167.png', NULL, '149', '2020-11-20 03:49:14', '2020-11-20 03:49:14'),
(4, 'bd53170fbcc524c73c53d25755f9dbac', 'Happy Paws Paradise Trading', 'Happy-Paws-Paradise-Trading', 'happyppts', 'https://api.whatsapp.com/send?phone=60108520885', 'happyppt175@gmail.com', '44, Happy PP Building, Jln Win, 01120 Selangor.', 'Selangor', NULL, 'Active', NULL, 'Happy Paws Paradise Trading Admin', '0128520885', NULL, NULL, '6', 'ASD, ZXC', '1, 2, 3', 'Breed 01, Breed 04', 'Other Info 1', 'Other Info 1, Other Info 2', '', '', 'bd53170fbcc524c73c53d25755f9dbac1605844314.png', 'bd53170fbcc524c73c53d25755f9dbac1605844320.png', NULL, '172', '2020-11-20 03:51:48', '2020-11-20 03:51:48'),
(5, 'b0353df94aade4ebd438b30eb3b3415c', 'JPuppies Corner', 'JPuppies-Corner', 'jpuppiescornerpg', 'https://api.whatsapp.com/send?phone=60107788997', 'jpuppiescorner@gmail.com', '34, JPC Building, Jln JPC, 12345 PG.', 'Penang', NULL, 'Active', NULL, 'JPuppies Corner Admin', '0127788997', NULL, NULL, '8', 'Cert 1, Cert 2, Cert 3', '2,5', 'Breed 1, Breed 3, Breed 5', 'Other Info 111', 'Other Info 222', 'Other Info 333', '', 'b0353df94aade4ebd438b30eb3b3415c1605844812.png', NULL, NULL, '0', '2020-11-20 04:00:06', '2020-11-20 04:00:06');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) NOT NULL,
  `service` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Grooming Services', 'Available', 1, '2020-07-03 01:45:10', '2020-07-03 02:07:36'),
(2, 'Pets Hotel / Boarding Services', 'Available', 1, '2020-07-03 01:45:34', '2020-07-03 02:15:12'),
(3, 'Stud Services', 'Available', 1, '2020-07-03 01:45:44', '2020-07-03 01:45:44'),
(4, 'Delivery Services / Pet Taxi', 'Available', 1, '2020-07-03 01:46:08', '2020-07-03 01:46:08'),
(5, 'Selling Pet Food &amp; Pet Accessories', 'Available', 1, '2020-07-03 01:46:28', '2020-07-03 01:46:28');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `img_name` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `uid`, `img_name`, `link`, `status`, `date_created`, `date_updated`) VALUES
(1, '052f3bd0866f1f6847471f054e005fd7', '1595496209052f3bd0866f1f6847471f054e005fd7.png', 'https://pariepets.com/dog.html', 'Show', '2020-05-15 02:23:59', '2020-07-23 09:23:29'),
(2, '27c4b80568e129104977f628cf1f33bf', '159549622827c4b80568e129104977f628cf1f33bf.png', 'https://pariepets.com/cat.html', 'Show', '2020-05-15 02:24:20', '2020-07-23 09:23:48'),
(3, '650fbc9ff8a916fcc1920fca983fa937', '650fbc9ff8a916fcc1920fca983fa9371589510512poster3.jpg', 'https://mypetslibrary.com/', 'Delete', '2020-05-15 02:41:52', '2020-07-08 01:14:17');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state_name`, `date_created`, `date_updated`) VALUES
(1, 'Johor', '2020-05-22 06:42:19', '2020-05-29 01:44:12'),
(2, 'Kedah', '2020-05-22 06:42:48', '2020-05-29 01:44:08'),
(3, 'Kelantan', '2020-05-22 06:42:48', '2020-05-29 01:44:03'),
(4, 'Negeri Sembilan', '2020-05-22 06:43:46', '2020-05-29 01:43:59'),
(5, 'Pahang', '2020-05-22 06:43:46', '2020-05-29 01:43:55'),
(6, 'Perak', '2020-05-22 06:44:16', '2020-05-29 01:43:52'),
(7, 'Perlis', '2020-05-22 06:44:16', '2020-05-29 01:43:50'),
(8, 'Selangor', '2020-05-22 06:44:37', '2020-05-29 01:43:45'),
(9, 'Terengannu', '2020-05-22 06:44:37', '2020-05-29 01:43:40'),
(10, 'Malacca', '2020-05-22 06:44:48', '2020-05-22 06:44:48'),
(11, 'Penang', '2020-05-22 06:44:48', '2020-05-22 06:44:48'),
(12, 'Sabah ', '2020-05-22 06:44:58', '2020-05-22 06:44:58'),
(13, 'Sarawak', '2020-05-22 06:44:58', '2020-05-22 06:44:58'),
(14, 'Kuala Lumpur', '2020-05-22 06:48:57', '2020-05-22 06:48:57'),
(15, 'Labuan', '2020-05-22 06:48:57', '2020-05-22 06:48:57'),
(16, 'Putrajaya', '2020-05-22 06:49:05', '2020-05-22 06:49:05');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `tac` varchar(255) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `fb_id` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `receiver_name` varchar(255) DEFAULT NULL,
  `receiver_contact_no` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_area` varchar(255) DEFAULT NULL,
  `shipping_postal_code` varchar(255) DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT 'profile-pic.jpg',
  `points` varchar(255) DEFAULT NULL,
  `favorite_puppy` text DEFAULT NULL,
  `favorite_kitten` text DEFAULT NULL,
  `favorite_reptile` text DEFAULT NULL,
  `favorite_product` text DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `name`, `email`, `country`, `phone_no`, `tac`, `password`, `salt`, `user_type`, `fb_id`, `birthday`, `gender`, `account_status`, `receiver_name`, `receiver_contact_no`, `shipping_state`, `shipping_area`, `shipping_postal_code`, `shipping_address`, `bank_name`, `bank_account_holder`, `bank_account_no`, `profile_pic`, `points`, `favorite_puppy`, `favorite_kitten`, `favorite_reptile`, `favorite_product`, `message`, `date_created`, `date_updated`) VALUES
(1, 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'admin@gmail.com', 'Malaysia', '123321', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-28 04:05:16', '2020-11-20 03:10:39'),
(2, '9349aede685bae49c49b0b895e465f6d', 'user', 'user@gmail.com', 'Malaysia', '123321', NULL, 'fb1343bf3ac191ede70b6d323ac4a19b10905f49ad1df13338c5b6a40ca33f54', 'e70e7e71ece4a3469028b5a4b2cf316bbb448d70', 1, NULL, NULL, NULL, NULL, 'Mr Tan Hooi Hooi', '0126012111', 'Perak', 'Taiping', '34000', '123, Jalan Durian, Kampung Rambutan,', 'HSBC', 'Oliver Queen', '1234455667', '1605842197.png', NULL, NULL, NULL, NULL, NULL, 'YES', '2020-04-28 04:05:55', '2021-01-06 08:15:54'),
(3, 'c2e4a20007e868502ea2857e094af93b', 'user2', 'user2@gmail.com', 'Singapore', '123321', NULL, 'fa0a1acac2e0844b2a9996cf8d57d86db163b04f7e93cdd157056992962273e4', '84d6588f07b2dd352d4e413a2dfe53fad13ff245', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-28 04:06:21', '2020-11-20 03:16:05'),
(4, 'c61de2ff2583ec2c9f8ce06db86a7a97', 'Wiskey', 'wiskey100@gmail.com', NULL, '0121234567', NULL, 'a231741b222e96c7c77589785a7260cb883a3272a982da2f71eb91321c8f06e0', '326342e1735583a8891a99442c2461bc7368babc', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-20 03:20:39', '2020-11-27 03:28:11'),
(5, '96efa305ea0de6aad93f3fe561f14bcc', 'Win Win Pet House', 'win125@gmail.com', NULL, '0107418520', NULL, 'bb2395b4c027ecf3d4b88f2c049bab6b1a3a41e401bd64202a63a20cead944a2', '6e97333a6ebb3c81dc0f07f501ed0853fe771945', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-20 03:43:46', '2020-11-27 03:28:22'),
(6, 'a55f9ab3c32ebbc3e0baaa00ba07303b', 'Durain Sam Gee Kann', 'duriansgk150@gmail.com', NULL, '0107419963', NULL, '0f7d3195f42127c7258c35b6fdebbb78a5749b7e5caa0ab52ca56db8fe952218', '48caa9c825eaf7236c615a9d5d19bea0c0029a13', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-20 03:49:14', '2020-11-27 03:28:36'),
(7, 'bd53170fbcc524c73c53d25755f9dbac', 'Happy Paws Paradise Trading', 'happyppt175@gmail.com', NULL, '0108520885', NULL, 'd7ded5b0b0f75df5dd4e98bdcccac835a4b25add8c1a631da2b27fd37950a8e1', '738bb9fca4213585f68487885b6df8d77eb6f528', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-20 03:51:48', '2020-11-27 03:28:47'),
(8, 'b0353df94aade4ebd438b30eb3b3415c', 'JPuppies Corner', 'jpuppiescorner@gmail.com', NULL, '0107788997', NULL, 'b12d14c1b3bd4ae1c0da027a0305340b120db315181c4b42ca6b33e0c3b81cf6', '67ea74dd56d6369b130f1dbc0dc03d500e87c514', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-20 04:00:06', '2020-11-27 03:29:01'),
(13, '5d6368155bcc2ec3b4234b250c737ac4', 'oliver', 'oliver@gmail.com', NULL, '01211223301', NULL, '9d2746484cc120ee6bea76da32669ecc8c23288831e5504df9ba216d0b2bd2e3', '3f3fe215c42dbdc32044da6292c4dd6d13d9d934', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-24 04:08:27', '2020-12-24 04:08:27'),
(14, 'fdefd9112e3579886dfbb9e04d7f928d', 'barry', 'barry@gmail.com', NULL, '01211223302', NULL, 'a173cab6b35b36641f45ab5706676034a52793976a83dc8133bfa571a4bf0bfb', 'a1d648f24973e4e1cb4b992b80056e5b62104293', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-24 04:10:34', '2020-12-24 04:10:34'),
(15, 'c2ec5c0906564aa40d80d5373253d0b6', 'kara', 'kara@gmail.com', NULL, '01211223303', NULL, 'da444f165c25e0e72e953d04192e8a61b4dc56d166806c9e5d74283c44d59a62', '5a2fad6e0572f35b8b39239d118cd92f24a27046', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-24 04:10:55', '2020-12-24 04:10:55'),
(16, 'd6ce9933b57bda792172b01b9aca8a7d', 'test', 'test@gmail.com', NULL, '123123123', NULL, '9b60ceee6810ac3c4e164f9d0dabe3052a29112d65b5aec72d22d2ad954e8cc6', '3e06b875e3188cc6dd4d114451d0e98a39306b50', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, 'YES', '2021-01-07 08:27:57', '2021-01-07 08:28:17'),
(17, '5633b78c7f79f1dfc93870d84c00abb3', 'micht', 'michelle@pariepets.com', NULL, '0164646483', NULL, '3c73cdb1a17b4efe4b6023e3113fe73dc384bc60f47d05ced48132d8b4082b10', '6ccb01f86cf188b300970e6c2bd96f7bebe1fa6d', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-08 06:06:03', '2021-01-08 06:06:03'),
(18, '97af0bfb9c1f8ca19db23ac31139d6bc', 'leon', 'leonkid88@hotmail.com', NULL, '01888888888', NULL, '1b125d9af393b97e1c2a3c2990936bd44f5e747898a635d584901db6bcb61be7', '999123c7d00301bb5756e2d9745814065a72c280', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-19 15:35:10', '2021-01-19 15:35:10'),
(19, '6bc414e3dd636553d27eb9dfd3ef974f', 'leonkid', 'leonkid88@hotmail.com', NULL, '0172742259', NULL, 'efe7f20e3516087bf7264a08954622da0865fa1ddbc040c5363720461cff9350', '27ac36a0b5d8e8372c2e7fbc4751414bfbb9e384', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-22 09:12:03', '2021-01-22 09:12:03'),
(20, 'dc0c19ea4deeb8d1e28e9064b6f8d7cf', 'titanlai', 'yn.lai@sudodevelopers.com', NULL, '0122664807', NULL, '98c5b7d76b7ec787abc9eb10af7078e9ba3991816292ca4677ebbc88ea033c73', '7de4a3f236960f5e12559aabd13155c105eb9e7b', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, 'YES', '2021-01-22 09:20:07', '2021-01-22 09:20:42');

-- --------------------------------------------------------

--
-- Table structure for table `vaccination`
--

CREATE TABLE `vaccination` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vaccination`
--

INSERT INTO `vaccination` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '1st Vaccination Done', 'Available', 1, '2020-06-25 01:40:17', '2020-06-25 01:40:17'),
(2, '2nd Vaccination Done', 'Available', 1, '2020-06-25 01:40:17', '2020-06-25 01:40:17'),
(3, '3rd Vaccination Done', 'Available', 1, '2020-06-25 01:40:45', '2020-06-25 01:40:45'),
(4, 'No', 'Available', 1, '2020-06-25 01:40:45', '2020-06-25 01:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `variation`
--

CREATE TABLE `variation` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT '0',
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `variation`
--

INSERT INTO `variation` (`id`, `uid`, `product_uid`, `name`, `price`, `amount`, `quantity`, `description`, `image`, `category`, `rating`, `status`, `date_created`, `date_updated`) VALUES
(1, 'dabda214d9c3df5f7d3e866782c1d4a4', '16b1bad9fbbf7b9322d0d08b26b5aa0a', 'iPhone 11', '3288', '3288.00', NULL, NULL, '1609906930iPhone11.jpg', 'Dog Toys and Accessory, Healthcare,Supplements', '0', 'Delete', '2021-01-06 04:22:10', '2021-01-06 06:39:51'),
(2, 'dc85597f49bf33a957d3cb8be0b38c79', '90efab9d88a5db154e7f908371e1f7cd', 'Parie Goat Milk Powder for Puppies', '70', '70.00', NULL, NULL, '1609909957milk-for-puppy2.png', 'Dog Wet Food', '0', 'Available', '2021-01-06 05:12:37', '2021-01-06 05:12:37'),
(3, '3196e420bc90b9611bc93d876168fbe3', '7ba69bf0cc5f01ae0562977fca09ea92', 'Parie Goat Milk Powder for Kittens', '70', '70.00', NULL, NULL, '1609910471milk-for-kitten.png', 'Cat Wet Food', '0', 'Available', '2021-01-06 05:21:11', '2021-01-06 05:21:11'),
(4, '5a80be29c265358a68547dd47d544b65', 'c634208e1ac402adcf6e372882ce99b3', 'INABA - Chic + Veg + Cheese', '6', '6.00', NULL, NULL, '1609913620twin2.jpg', 'Dog Wet Food', '0', 'Available', '2021-01-06 06:03:12', '2021-01-06 06:13:40'),
(5, 'ee999324a673c80ced247b6df99d15cc', 'c634208e1ac402adcf6e372882ce99b3', 'INABA Chic + Veg + Beef', '6', '6.00', NULL, NULL, '1609913639twin3.jpg', 'Dog Wet Food', '0', 'Available', '2021-01-06 06:03:44', '2021-01-06 06:14:00'),
(6, '0b8889fd94a0e714e8ec0176fa10b042', 'c634208e1ac402adcf6e372882ce99b3', 'INABA Chic + Veg', '6', '6.00', NULL, NULL, '1609913652twin4.jpg', 'Dog Wet Food', '0', 'Available', '2021-01-06 06:04:48', '2021-01-06 06:14:12'),
(7, '54c8ac1ed6d364f6a25d90317657800d', '0f7b32cdabccd5a533bea31c743144ab', 'Royal Canin British Short Hair Kitten 2Kg', '80', '80.00', NULL, NULL, '1609914420royal.jpg', 'Cat Dry Food', '0', 'Available', '2021-01-06 06:27:00', '2021-01-06 06:27:00'),
(8, '5cff63e3ad7dbbc875748cac752eb15f', 'de6b3e53200445d688f5797620062c9b', 'Harrington&#039;s Dog Food Complete Turkey And Vegetables', '100', '100.00', NULL, NULL, '1609915462ha1.jpg', 'Dog Dry Food', '0', 'Available', '2021-01-06 06:44:22', '2021-01-06 06:44:22'),
(9, '8721fd36935f59c771b25697b05c280f', 'c42d1def318a860ee24cbd2d25805c23', 'admin', '70', '70.00', NULL, NULL, '16099252204paws.png', 'Dog Dry Food', '0', 'Available', '2021-01-06 09:27:00', '2021-01-06 09:27:00'),
(10, 'afcaaf2ce538b567eea0fb5407802fe4', '2fcab53d2235eff90661cbd69c95ac1e', 'AB', '888', '888.00', NULL, NULL, '1617813256frog.jpeg', 'Dog Wet Food', '0', 'Available', '2021-04-07 16:34:16', '2021-04-07 16:34:16'),
(11, 'bde407060a121198f7867ae12291f86f', '2fcab53d2235eff90661cbd69c95ac1e', 'BBBASAD', '66', '66.00', NULL, NULL, '1617813282frog.jpeg', 'Dog Wet Food', '0', 'Available', '2021-04-07 16:34:42', '2021-04-07 16:34:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `breed`
--
ALTER TABLE `breed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_card`
--
ALTER TABLE `credit_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fb_login`
--
ALTER TABLE `fb_login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kitten`
--
ALTER TABLE `kitten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_details`
--
ALTER TABLE `pet_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preorder_list`
--
ALTER TABLE `preorder_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `puppy`
--
ALTER TABLE `puppy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reported_article`
--
ALTER TABLE `reported_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reported_reviews`
--
ALTER TABLE `reported_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reptile`
--
ALTER TABLE `reptile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews_respond`
--
ALTER TABLE `reviews_respond`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`name`);

--
-- Indexes for table `vaccination`
--
ALTER TABLE `vaccination`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variation`
--
ALTER TABLE `variation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `breed`
--
ALTER TABLE `breed`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `credit_card`
--
ALTER TABLE `credit_card`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fb_login`
--
ALTER TABLE `fb_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kitten`
--
ALTER TABLE `kitten`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pet_details`
--
ALTER TABLE `pet_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `preorder_list`
--
ALTER TABLE `preorder_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `puppy`
--
ALTER TABLE `puppy`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `reported_article`
--
ALTER TABLE `reported_article`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reported_reviews`
--
ALTER TABLE `reported_reviews`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reptile`
--
ALTER TABLE `reptile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reviews_respond`
--
ALTER TABLE `reviews_respond`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `seller`
--
ALTER TABLE `seller`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `vaccination`
--
ALTER TABLE `vaccination`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `variation`
--
ALTER TABLE `variation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
