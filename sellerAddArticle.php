<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';
  
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
// $sellerName = $userData->getName();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Article | The company" />
<title>Add Article | The company</title>
<meta property="og:description" content="The company serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into The company to search for their preferred breed or getting advice from us." />
<meta name="description" content="The company serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into The company to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="The company, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">

  <div class="width100">
    <h1 class="green-text h1-title">Add New Article</h1>
    <div class="green-border"></div>
  </div>

  <div class="border-separation">

    <div class="clear"></div>
    <!-- <form> -->
    <form action="utilities/sellerAddArticlesFunction.php" method="POST" enctype="multipart/form-data">

      <!-- <div class="dual-input"> -->
      <div class="width100 overflow">
        <p class="input-top-p admin-top-p">Title*</p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="Title" name="title" id="title" required>      
      </div>

      <!-- <div class="dual-input second-dual-input">
        <p class="input-top-p admin-top-p">Status*</p>
        <select class="input-name clean admin-input" name="title" id="title" required>
          <option>Publish/Approved</option>
          <option>Not Available</option>
        </select>     
      </div>     -->

      <div class="clear"></div>

      <div class="width100 overflow">
        <p class="input-top-p admin-top-p">Article Slug/Link* (or URL, can't repeat,  Avoid Spacing and Symbol Specially"',.) Can Use -  <img src="img/attention2.png" class="attention-png opacity-hover open-url" alt="Click Me!" title="Click Me!"></p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="Article Slug/Link" name="article_link" id="article_link" required>              	
      </div>

      <div class="clear"></div>

      <div class="width100 overflow">
        <p class="input-top-p admin-top-p">Keyword (Use Coma , to Separate Each Keyword, Avoid"')  <!--<img src="img/attention2.png" class="attention-png opacity-hover open-keyword" alt="Click Me!" title="Click Me!">--></p>
        <textarea class="input-name clean input-textarea admin-input keyword-input" type="text" placeholder="cute,malaysia,pet,dog,puppy," name="keyword_two" id="keyword_two" required></textarea>  	
      </div>        

      <div class="clear"></div>  

      <div class="width100 overflow">
        <p class="input-top-p admin-top-p">Upload Cover Photo  (Less Than 1.8mb)</p>
        <input id="file-upload" type="file" name="cover_photo" id="cover_photo" accept="image/*" required>    
        <!-- Crop Photo Feature --> 	
      </div>        

      <div class="clear"></div>

      <div class="width100 overflow ow-margin-top20">
        <p class="input-top-p admin-top-p">Photo Source/Credit <!--<img src="img/attention2.png" class="attention-png opacity-hover open-url" alt="Click Me!" title="Click Me!">--></p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="Photo Source:" name="cover_photo_source" id="cover_photo_source">              	
      </div>        

      <div class="clear"></div>

      <div class="width100 overflow">
        <p class="input-top-p admin-top-p">Article Summary/Description (Won't Appear inside the Main Content, Avoid "') <img src="img/attention2.png" class="attention-png opacity-hover open-desc" alt="Click Me!" title="Click Me!"></p>
        <textarea class="input-name clean input-textarea admin-input keyword-input" type="text" placeholder="Article Summary" name="descprition" id="descprition" required></textarea>  	
      </div>        

      <div class="clear"></div>      

      <div class="form-group publish-border input-div width100 overflow">
        <p class="input-top-p admin-top-p">Main Content (Avoid "' and don't copy paste image inside, click the icon for upload image/video/link tutorial)<img src="img/attention2.png" class="attention-png opacity-hover open-tutorial" alt="Click Me!" title="Click Me!"></p>
        <textarea name="editor" id="editor" rows="10" cols="80"  class="input-name clean input-textarea admin-input editor-input" ></textarea>
      </div>    

      <div class="clear"></div>    

      <div class="width100 overflow text-center">     
        <button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
      </div>

    </form>
  </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    CKEDITOR.replace('editor');
</script>

</body>
</html>